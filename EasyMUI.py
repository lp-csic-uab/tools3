#!/usr/local/bin/python2.7
# encoding: utf-8
"""
EasyMUI -- Easy to use Multiple User Interface

:synopsis:   EasyMUI is a package for making applications/scripts with mixed
CLI and GUI User Interfaces. By now, it uses the EasyGUI package for the
graphical dialogs.

It defines the following classes and functions:
    :class:`CLIError`
    :class:`ProgramCancelled`
    :class:`CLINonBlockingMsgBox` 
    :class:`EasyGUINonBlockingMsgBox`
    :class:`UIDialogsAbstract`
    :class:`CLIDialogs`
    :class:`EasyGUIDialogs`
    :class:`Configuration`
    :func:`get_folder_files`
    :func:`get_list_files`
    :func:`filter_strings_by_re`
    :func:`csv_dict_writer`
    

:created:    2014-01-13

:author:     Óscar Gallardo (ogallardo@protonmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2013 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '0.6'
__updated__ = '2014-07-08'


#===============================================================================
# Imports
#===============================================================================
from __future__ import division
from __future__ import print_function

import json
from importlib import import_module
from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
import sys
import os
import csv
import re
import glob
from collections import OrderedDict


#===============================================================================
# Class definitions
#===============================================================================
class CLIError(Exception):
    """
    Generic sub-class of :class:``Exception`` to raise and log different fatal 
    errors.
    """
    _msg = 'CLIError'
    
    def __init__(self, msg=None):
        super(CLIError).__init__(type(self))
        if msg:
            self._msg = '{0}: {1}'.format(self._msg, msg)
    
    @property
    def msg(self):
        return self._msg
    
    def __str__(self):
        return self.msg
    
    def __unicode__(self):
        return self.msg


class ProgramCancelled(Exception):
    """
    Generic sub-class of :class:``Exception`` to raise when program must be 
    cancelled by an user action.
    """
    _msg = 'Program Cancelled by user'
    
    def __init__(self, msg=None):
        super(ProgramCancelled).__init__(type(self))
        if msg:
            self._msg = '{0}: {1}'.format(self._msg, msg)
    
    @property
    def msg(self):
        return self._msg
    
    def __str__(self):
        return self.msg
    
    def __unicode__(self):
        return self.msg


class CLINonBlockingMsgBox(object):
    def __init__(self, msg='', title='', *args, **kwargs):
        self._alive = False
        self._msg = msg
        self._title = title
    
    @property
    def msg(self):
        return self._msg
    @msg.setter
    def msg(self, msg):
        self.change_msg_and_title(msg=msg)
    
    @property
    def title(self):
        return self._title
    @title.setter
    def title(self, title):
        self.change_msg_and_title(title=title)
    
    def change_msg_and_title(self, msg=None, title=None):
        if msg:
            self._msg = msg
        if title:
            self._title = title
        CLIDialogs.msgbox(self._msg, title)
        return self
    
    def is_alive(self):
        return self._alive
    
    def isAlive(self):
        return self.is_alive()
    
    def start(self):
        self._alive = True
        return self.run()
    
    def run(self):
        return CLIDialogs.msgbox(self._msg, self._title)
        
    def close(self):
        self._alive = False
    
    def cancel(self):
        raise ProgramCancelled()


class EasyGUINonBlockingMsgBox(CLINonBlockingMsgBox):
    def __init__(self, msg='', title='', root=None):
        super(EasyGUINonBlockingMsgBox, self).__init__(msg, title)
#        self._alive = False
#        self._msg = msg
#        self._title = title
        self.root = root
        self._msgwdgt = None
        self._boxRoot = None
    
    def change_msg_and_title(self, msg=None, title=None):
        if self.is_alive() and self._boxRoot:
            if msg:
                self._msg = msg
                self._msgwdgt.configure(text=msg)
            if title:
                self._title = title
                self._boxRoot.title(title)
            self._boxRoot.update()
    
    @property
    def msg(self):
        return self._msg
    @msg.setter
    def msg(self, msg):
        self.change_msg_and_title(msg=msg)
    
    @property
    def title(self):
        return self._title
    @title.setter
    def title(self, title):
        self.change_msg_and_title(title=title)
    
    def run(self):
        """
        Display ``self.msg`` and ``self.title``
        """
        import easygui as eg

        # Define the root window:
        root = self.root
        if root:
            root.withdraw()
            eg.boxRoot = eg.Toplevel(master=root)
            eg.boxRoot.withdraw()
        else:
            eg.boxRoot = eg.Tk()
            eg.boxRoot.withdraw()
        self._boxRoot = eg.boxRoot
        # Define the root window properties:
        eg.boxRoot.protocol('WM_DELETE_WINDOW', eg.denyWindowManagerClose )
        eg.boxRoot.title(self.title)
        eg.boxRoot.iconname('Dialog')
        eg.boxRoot.geometry(eg.rootWindowPosition)
        eg.boxRoot.minsize(400, 50)
        # Define the messageFrame:
        messageFrame = eg.Frame(master=eg.boxRoot)
        messageFrame.pack(side=eg.TOP, fill=eg.BOTH)
        # Place the widgets in the frames:
        messageWidget = eg.Message(messageFrame, text=self.msg, width=400)
        self._msgwdgt = messageWidget
        messageWidget.configure( font=(eg.PROPORTIONAL_FONT_FAMILY, 
                                       eg.PROPORTIONAL_FONT_SIZE) )
        messageWidget.pack(side=eg.TOP, expand=eg.YES, fill=eg.X, padx='3m', 
                           pady='3m')
        # Show the message in its window:
        eg.boxRoot.deiconify()
        eg.boxRoot.update()
    
    def close(self):
        if self.is_alive() and self._boxRoot:
            self._boxRoot.destroy()
            if self.root: 
                self.root.deiconify()
            self._alive = False
    
    def cancel(self):
        self.close()
        raise ProgramCancelled()


class UIDialogsAbstract(object):
    """
    Abstract class for User Interface Dialogs
    """
    def __init__(self):
        self._gui = None
        self.actual_nonblockingmsgbox = None
    
    @property
    def gui(self):
        return self._gui
    
    def gui_toolkit_suport(self, guitoolkit_name):
        """
        Test if the system has GUI support for the given GUI Toolkit name
        (guitoolkit_name).
        """
        if os.name == 'posix' and not os.environ.get('DISPLAY', None):
            self.gui_module = None
        else:
            try:
                self.gui_module = import_module(guitoolkit_name)
            except ImportError:
                self.gui_module = None
        return self.gui_module
    
    def print(self, *args, **kwargs):
        return self.msgbox( *args, **kwargs)
    
    def msgbox(self, text='', title=None, *args, **kwargs):
        raise NotImplemented
    
    def textbox(self, msg='', title=' ', text='', codebox=0):
        raise NotImplemented
    
    def _nonblockingmsgbox(self, text='', title=None, Cls=None, *args, **kwargs):
        # Only allow an active nonblockingmsgbox:
        if self.actual_nonblockingmsgbox and self.actual_nonblockingmsgbox.is_alive():
            self.actual_nonblockingmsgbox.change_msg_and_title(text, title)
        else:
            nonblockingmsgbox = Cls(text, title, *args, **kwargs)
            nonblockingmsgbox.start()
            self.actual_nonblockingmsgbox = nonblockingmsgbox
        return self.actual_nonblockingmsgbox
    
    def nonblockingmsgbox(self, text='', title=None, *args, **kwargs):
        raise NotImplemented
    
    def inputbox(self, text='', title=None, default=None, *args, **kwargs):
        raise NotImplemented
    
    def multi_inputbox(self, text='', title=None, fields2defaults=None, 
                       **kwargs):
        raise NotImplemented
        
    def selectfiles(self, msg='', title=None, default=None, folder=None, 
                    recursive=False, followlinks=False, include_re=None, 
                    exclude_re=None):
        raise NotImplemented
    
    def selectfolder(self):
        raise NotImplemented
    
    def fileopenbox(self, msg='', title=None, default=None, filetypes=None):
        raise NotImplemented
    
    def filesavebox(self, msg='', title=None, default=None, filetypes=None):
        raise NotImplemented
    
    def choicebox(self, msg='', title=None, choices=None):
        raise NotImplemented
    
    def multi_choicebox(self, msg='', title=None, choices=None):
        raise NotImplemented
    
    def buttonbox(self, msg='', title=None, choices=None):
        raise NotImplemented
    
    def ynbox(self, msg='Shall I continue?', title=None, choices=('Yes', 'No')):
        raise NotImplemented


class CLIDialogs(UIDialogsAbstract):
    """
    Command Line Interface Dialogs
    """
    def __init__(self):
        self._gui = False
        self.actual_nonblockingmsgbox = None
        #
        try:
            self.print = globals()['__builtins__'].print #Standard interpreter
        except AttributeError:
            self.print = globals()['__builtins__']['print'] #Make it work in some situations

    def gui_toolkit_suport(self, guitoolkit_name):
        self.gui_module = None
        self.cli_module = self
        return self.gui_module
    
    @staticmethod
    def msgbox(text='', title=None, *args, **kwargs):
        if title:
            print( '\n-= {0} =-\n'.format(title) )
        if text:
            print( '{0}\n'.format(text) )
        return
    
    def textbox(self, msg='', title=' ', text='', codebox=0):
        if msg:
            text = msg + '\n' + text
        return self.msgbox(text, title)

    def nonblockingmsgbox(self, text='', title=None, *args, **kwargs):
        return self._nonblockingmsgbox(text, title, CLINonBlockingMsgBox)
    
    @staticmethod
    def inputbox(text='', title=None, default=None, *args, **kwargs):
        if title:
            print( '\n-= {0} =-\n'.format(title) )
        if default:
            text = '{0} [{1}]'.format(text, default)
            
        resp = raw_input( '{0} : '.format(text) )
        if not resp:
            return default
        else:
            return resp
    
    def multi_inputbox(self, text='', title=None, fields2defaults=None, 
                       **kwargs):
        if fields2defaults is None:
            fields2defaults = OrderedDict()
        if kwargs:
            fields2defaults.update(kwargs)
                
        self.msgbox(text, title)
        fields2responses = OrderedDict()
        for field, default in fields2defaults.items():
            fields2responses[field] = self.inputbox(field, None, str(default))
        return fields2responses
    
    def selectfiles(self, msg='', title=None, default=None, folder=None, 
                    recursive=False, followlinks=False, include_re=None, 
                    exclude_re=None):
        if not folder:
            folder = os.getcwd()
        file_choices = get_list_files((folder,), recursive, followlinks)
        file_choices = filter_strings_by_re(file_choices, include_re, exclude_re)
        if not file_choices:
            return None
#        self.msgbox( 'Files in {0}'.format(folder) )
        selected_files = self.multi_choicebox(msg, title, file_choices)
        if selected_files:
            # ``selected_files`` treatment:
            selected_files = [os.path.join(folder, filen) for filen in selected_files]
            selected_files = get_list_files(selected_files, recursive, followlinks)
            selected_files = filter_strings_by_re(selected_files, include_re, exclude_re)
            return selected_files
        else:
            return None
    
    def fileopenbox(self, msg='', title=None, default=None, filetypes=None):
        return self.inputbox(msg, title, default)
    
    def filesavebox(self, msg='', title=None, default=None, filetypes=None):
        return self.inputbox(msg, title, default)
    
    def _get_choicebox_vars(self, msg='', choices=None):
        if not choices:
            raise TypeError('No choices provided')
        #
        choices_n = len(choices)
        choices_msg = list()
        for index, choice in enumerate( sorted(choices) ):
            choices_msg.append( '  {0} .- {1}'.format(index+1, choice) )
        choices_msg.append('  0 .- To Cancel')
        valid_resp = { str(index) for index in range(choices_n+1) }
        input_msg = '{0} (from 0 to {1})'.format(msg, choices_n)
        #
        return '\n'.join(choices_msg), valid_resp, input_msg
    
    def choicebox(self, msg='', title=None, choices=None, **kwargs):
        choices_msg, valid_resp, input_msg = self._get_choicebox_vars(msg, 
                                                                      choices)
        self.msgbox(choices_msg, title)
        resp = None
        while resp not in valid_resp:
            resp = self.inputbox(input_msg, default='0')
        if resp != '0':
            return choices[int(resp)-1]
        else:
            return None
    
    def multi_choicebox(self, msg='', title=None, choices=None, **kwargs):
        choices_msg, valid_resp, input_msg = self._get_choicebox_vars(msg, 
                                                                      choices)
        self.msgbox(choices_msg, title)
        resps = (-1,)
        while not valid_resp.issuperset(resps):
            resps = self.inputbox(input_msg, default='1')
            resps = set(re.split(' and |, |,| ', resps))
            expand_resps = set()
            for resp in resps:
                if '-' in resp[1:-1]: #resp[1:-1] avoids working with negative numbers (such as '-2') and responses like '2-'
                    range_resp = resp.split('-')
                    try:
                        limits = ( int(range_resp[0]), int(range_resp[-1]) )
                        range_resp = [ str(index) for index in range(min(limits), max(limits)+1) ]
                    except ValueError: #Generally a int parameter was not a number
                        pass
                    expand_resps.update(range_resp)
                else:
                    expand_resps.add(resp)
            resps = expand_resps
        if '0' not in resps:
            return [ choices[int(resp)-1] for resp in resps ]
        else:
            return None
    
    def buttonbox(self, msg='', title=None, choices=None):
        schoices = [str(choice) for choice in choices]
        valid_resp2index = dict()
        for func in (lambda s: s, str.upper, str.lower, lambda s: s[0].upper(),
                     lambda s: s[0].lower()):
            for index, choice in enumerate(schoices):
                key = func(choice)
                if not valid_resp2index.has_key(key):
                    valid_resp2index[key] = index
        resp = None
        msg = '{0} ({1})'.format( msg, '/'.join(schoices) )
        while not valid_resp2index.has_key(resp):
            resp = self.inputbox(msg, title, schoices[0])
        return schoices[ valid_resp2index[resp] ]
    
    def ynbox(self, msg='Shall I continue?', title=None, choices=('Yes', 'No')):
        choices = choices[:2]
        resp = self.buttonbox(msg, title, choices)
        return 1 - choices.index(resp)


class EasyGUIDialogs(UIDialogsAbstract):
    """
    Graphic User Interface Dialogs
    """
    def __init__(self):
        self._gui = None
        self.actual_nonblockingmsgbox = None
        self.cli_module = CLIDialogs()
        self.gui_toolkit_suport('easygui')
        
        if self.gui_module:
            self.turn_on_gui()
        else:
            self.turn_off_gui()
    
    def _multi_inputbox(self, text='', title=None, fields2defaults=None, 
                        **kwargs):
        if fields2defaults is None:
            fields2defaults = OrderedDict()
        if kwargs:
            fields2defaults.update(kwargs)
                
        fields = fields2defaults.keys()
        defaults = [fields2defaults[field] for field in fields]
        responses = self.gui_module.multenterbox(msg=text, title=title, 
                                                 fields=fields, values=defaults)
        if responses is not None:
            return OrderedDict( zip(fields, responses) ) #Ok pressed
        else:
            return None #Cancel pressed
    
    def _selectfiles(self, msg='', title=None, default=None, folder=None, #REFACTORIZE
                     recursive=False, followlinks=False, include_re=None, 
                     exclude_re=None):
        if not folder:
            folder = os.getcwd()
        file_choices = get_list_files((folder,), recursive, followlinks)
        file_choices = filter_strings_by_re(file_choices, include_re, exclude_re)
        selected_files = self.gui_module.multchoicebox(msg, title, file_choices)
        return selected_files
    
    def _print(self, value, sep=' ', end='\n', file=None, *args):
        args = (value,) + args
        msg = sep.join(args) + end
        title = 'Information'
        if file and file is not sys.stdout:
            self.cli_module.print(*args, sep=sep, end=end, file=file)
        return self.gui_module.msgbox(msg, title)
    
    def _nonblockingmsgbox(self, text='', title=None, *args, **kwargs):
        return super(EasyGUIDialogs, self)._nonblockingmsgbox(text, title, 
                                                              EasyGUINonBlockingMsgBox, 
                                                              *args, **kwargs)
    
    def _gui_setter(self, turn_on):
        if turn_on is not self._gui:
            if turn_on and self.gui_module:
                self.msgbox = self.gui_module.msgbox
                self.textbox = self.gui_module.textbox
                self.nonblockingmsgbox = self._nonblockingmsgbox
                self.inputbox = self.gui_module.enterbox
                self.multi_inputbox = self._multi_inputbox
                self.selectfolder = self.gui_module.diropenbox
                self.selectfiles = self._selectfiles
                self.fileopenbox = self.gui_module.fileopenbox
                self.filesavebox = self.gui_module.filesavebox
                self.choicebox = self.gui_module.choicebox
                self.multi_choicebox = self.gui_module.multchoicebox
                self.buttonbox = self.gui_module.buttonbox
                self.ynbox = self.gui_module.ynbox
                self.print = self._print
                self._gui = True
            else:
                self.msgbox = self.cli_module.msgbox
                self.textbox = self.cli_module.textbox
                self.nonblockingmsgbox = self.cli_module.nonblockingmsgbox
                self.inputbox = self.cli_module.inputbox
                self.multi_inputbox = self.cli_module.multi_inputbox
                self.selectfolder = self.cli_module.inputbox
                self.selectfiles = self.cli_module.selectfiles
                self.fileopenbox = self.cli_module.fileopenbox
                self.filesavebox = self.cli_module.filesavebox
                self.choicebox = self.cli_module.choicebox
                self.multi_choicebox = self.cli_module.multi_choicebox
                self.buttonbox = self.cli_module.buttonbox
                self.ynbox = self.cli_module.ynbox
                self.print = self.cli_module.print
                self._gui = False
        return self
    
    def turn_off_gui(self):
        self._gui_setter(False)
        return self
    
    def turn_on_gui(self):
        self._gui_setter(True)
        return self
    
    def gui_and_cli_print(self, *args, **kwargs):
        self.cli_module.print(*args, **kwargs)
        return self._print(*args, **kwargs)

    
class Configuration(object):
    """
    Class used to set configuration options from various sources (command line, 
    configuration file, or interactively)
    """
    _extra_config_file_attrs = tuple() # Extra attributes, not in self._config_params_order, to save/load to/from a config file
    
    def __init__(self, config_params, config_params_order=None, 
                 config_file=None, version=None, created=None, updated=None):
        """
        Constructor
        
        :param dictionary/OrderedDict config_params: a dictionary or OrderedDict 
        of the type:
        {'attribute_name': {'arg': ('-CLI_argument_form1', '--CLI_argument_form2),
                            'arg_opts' {key value pairs of options for this argument to pass to ArgumentParser instance in ``self.parser``}
                            },
        ...}
        
        :param list config_params_order: 
        :param string config_file: name of a file containing the configuration values to use.
        """
        self._ui = EasyGUIDialogs()
        self.prg_name = os.path.splitext( os.path.basename( sys.argv[0] ) )[0]
        self.version = version
        self.created = created
        self.updated = updated
        
        # Mandatory parameters for all instances of the class:
        prg_version_msg = '{0} v{1} ({2})'.format(self.prg_name, self.version, 
                                                  self.updated)
        all_config_params = OrderedDict( ( 
                 ('version', {'arg': ('-V', '--version'),
                              'arg_opts': dict(action='version', 
                                               version=prg_version_msg)
                              } ),
                 ('force_cli', {'arg': ('--force-cli',),
                                'arg_opts': dict(action='store_true', 
                                                 help='force command line interface (no GUI)')
                                } ),
                                            ) )
        # Plus instance-defined parameters:
        all_config_params.update(config_params)
        # Order of the arguments:
        if not config_params_order:
            config_params_order = all_config_params.keys()
        else:
            for mandatory_arg in ('force_cli', 'version'):
                if not mandatory_arg in config_params_order:
                    config_params_order.insert(0, mandatory_arg)
        self._config_params_order = config_params_order
        
        # Get instance attributes from `config_file` configuration values:
        if config_file:
            self.load_config(config_file)
        
        # Get instance attributes from command-line argument values:
        self.__init_attr_from_cli__(all_config_params)
    
    def __init_attr_from_cli__(self, config_params):
        # Get some extra program information:
        prg_desc = globals()['__doc__'].split('\n')[1]
        prg_license = '''{0}
    
      Created by Óscar Gallardo on {1}.
      Copyright 2013 LP-CSIC/UAB. All rights reserved.
    
      Licensed under the GPLv3
      https://www.gnu.org/licenses/gpl-3.0.html
    
      Distributed on an 'AS IS' basis without warranties
      or conditions of any kind, either express or implied.
    
    USAGE
    '''.format(prg_desc, self.created)
        
        # Setup arguments for ``self.parser``:
        self.parser = ArgumentParser(description=prg_license, 
                                     formatter_class=RawDescriptionHelpFormatter)
        for attr in self._config_params_order:
            self.parser.add_argument(*config_params[attr]['arg'], 
                                     dest=attr, 
                                     **config_params[attr]['arg_opts'])
        # Process arguments:
        parsed_args = self.parser.parse_args()
        # Fill self instance attributes with argument values from ``self.parser``:
        config_params_order = self._config_params_order[:]
        config_params_order.remove('version') #Program's version is no part of the instance attributes
        for attr in config_params_order:
            setattr(self, attr, getattr(parsed_args, attr))
    
    @property
    def ui(self):
        if self.force_cli and self._ui.gui:
            self._ui.turn_off_gui()
        return self._ui
    
    def load_config(self, config_file=None):
        if config_file:
            self.config_file = config_file
        # Load attribute names and their values from JSON config file:
        if os.path.exists(self.config_file):
            with open(self.config_file, 'rU') as io_file:
                loaded_attr2val = json.load(io_file, 
                                            object_pairs_hook=OrderedDict)
        else:
            return None        
        # Fill self instance attributes with values from config file: 
        config_file_attrs = ( self._config_params_order + 
                              list(self._extra_config_file_attrs) )
        for attr in config_file_attrs:
            setattr(self, attr, loaded_attr2val.get(attr, None))
        
        return loaded_attr2val
    
    def save_config(self, config_file=None):
        if config_file:
            self.config_file = config_file
        # Prepare attributes->values dictionary to save, with data from self 
        # instance attributes:
        save_attr2val = dict()
        config_file_attrs = ( self._config_params_order + 
                              list(self._extra_config_file_attrs) )
        for attr in config_file_attrs:
            save_attr2val[attr] = getattr(self, attr, None)
        # Save attribute names and their values to a JSON config file:
        with open(self.config_file, 'wb') as io_file:
            json.dump(save_attr2val, io_file, indent=4)
        
        return self


#===============================================================================
# Function definitions
#===============================================================================
def get_folder_files(folder, recursive=False, followlinks=False):
    """
    Get all the file names inside a `folder`.
    
    :param string folder: Folder full path
    :param bool recursive: If True, files in subfolders will be also gathered.
    :param bool followlinks: If False, files or folders symlinks will be
    filtered out.
    
    :return: a list of files (with their path/`folder`).
    """
    folder_files = list()
    if not recursive:
        # Add only files (with their path/`folder`) to ``folder_files``,
        # taking into account ``followlinks``:
        for filen in os.listdir(folder):
            filen = os.path.join(folder, filen)
            if not os.path.isdir(filen) and (followlinks or 
                                             not os.path.islink(filen)):
                folder_files.append(filen)
    else:
        for rootfolder, subfolders, files in os.walk(folder):
            if not followlinks:
                # In-situ elimination of symlinks from ``subfolders``, to avoid
                # ``os.walk`` to traverse them:
                for subfolder in reversed(subfolders):
                    if os.path.islink(subfolder):
                        subfolders.remove(subfolder)
            # Add files (with their path/`folder`) to ``folder_files``, taking
            # into account ``followlinks``:
            for filen in files:
                filen = os.path.join(rootfolder, filen)
                if followlinks or not os.path.islink(filen):
                    folder_files.append(filen)
    return folder_files


def get_list_files(filesandfolders, recursive=False, followlinks=False):
    """
    Gathers a list of file names from a list of globs, files and folders
    (`filesandfolders`), gathering the files inside each of the folders.
    
    :param list filesandfolders: A list of globs, files and folders.
    :param bool recursive: If True, files in subfolders will be also gathered.
    :param bool followlinks: If False, files or folders symlinks will be
    filtered out.
    
    :return: a list of files (with their path/`folder`).
    """
    files = list()
    for fileorfolder in filesandfolders:
        if not os.path.exists(fileorfolder): #Globs and non existing files/folders
            if '*' in fileorfolder or '?' in fileorfolder or '[' in fileorfolder: #Globs
                glob_files = glob.glob(fileorfolder)
                glob_files = get_list_files(glob_files, recursive, followlinks) # CAUTION! using recursion
                files.extend(glob_files)
            else: #Filter non existing files/folders
                continue
        if not followlinks and os.path.islink(fileorfolder): #Filter links
            continue
        if os.path.isdir(fileorfolder): #Folder
            folder_files = get_folder_files(fileorfolder, recursive, followlinks)
            files.extend(folder_files)
        else: #File
            files.append(fileorfolder)
    return files


def filter_strings_by_re(strings, include_re=None, exclude_re=None):
    """
    Filter a list of strings according to regex in excluded regex list
    (exclude_re) and/or included regex list (include_re)
    """
    filtered_strings = strings[:]
    # Discard strings matching a regex in excluded regex list:
    if exclude_re:
        for ex_re in exclude_re:
            filtered_strings = [each for each in filtered_strings 
                                if not re.search(ex_re, each)]
    # Discar strings not matching a regex in included regex list:
    if include_re:
        for in_re in include_re:
            filtered_strings = [each for each in filtered_strings 
                                if re.search(in_re, each)]        
    #
    return filtered_strings


def csv_dict_writer(filen, dictrows, fieldnames=None, title=None):
    if not fieldnames:
        fieldnames = sorted(dictrows[0].keys())
    with open(filen, 'wb') as io_file:
        if title is not None:
            io_file.write(title + '\n\n')
        csvdw = csv.DictWriter(io_file, fieldnames=fieldnames, delimiter='\t')
        csvdw.writeheader()
        csvdw.writerows(dictrows)



if __name__ == '__main__':
    pass
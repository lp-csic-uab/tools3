# Search in a KimBlast-generated indexed sqlite file (dbase.idx) from a
# given sequence database all the peptide sequences in a text file.
# Generates an excel file containing coverage data for each protein represented
# in the peptide collection

import os
from easygui import fileopenbox as AskFileForOpen
from easygui import msgbox as Message
# ProgressBar
from collections import defaultdict
from kimblast.kblast_cls_basedbase import BaseDataBase
from kimblast.kblast_funcs import digest
from commons.mass import get_mass


def get_sec_list(arch):
    secs = []
    for line in open(arch, 'r'):
        secs.append(line.strip())
    return secs

def refine_matches(match):
    """Remove overlapping im matches.

    match:   [(start, end),(start2, end2)....]
    returns: [(start, end2)....]
    """
    booked = []
    new_match = []
    end = 0
    n = 0
    for index in range(len(match)):
        booked.extend(range(match[index][0], match[index][1]))

    booked = set(booked)
    booked = list(booked)
    booked.sort()
    #print booked

    while n < len(booked):
        #print n
        start = booked[n]
        while n < len(booked) - 1:
            if booked[n + 1] == booked[n] + 1:
                end = booked[n + 1]
                n += 1
            else:
                end = booked[n]
                break
        new_match.append((start, end + 1))
        n += 1

    #print new_match
    return new_match

def build_protein_coverages_dict(seqs, dbase):
    """Build a dictionary of protein references and matched sequences

    Build the dictionary from a database of protein sequences and a list of
    peptide sequences.
    Dict keys :  protein accession
    Dict values: list of pairs (start, end) of matched peptides:
                   [(2, 5), (10, 15), ...] (end is not inclusive)

    """
    protein_coverages = defaultdict(list)
    maxim = len(seqs)
    #progress = ProgressBar("WAIT, I AM WORKING", maxim)
    for seq in seqs:
        for item in dbase:
            if seq in item[2]:  #
                end = 0
                while True:
                    start = item[2].find(seq, end)
                    if start == -1:
                        break
                    end = start + len(seq)
                    protein_coverages[item[0]].append((start, end))
                #print item
        #progress.inc()
    #
    # del progress
    return protein_coverages

def save_dict():
    """save to work later"""
    pass

def get_seq_with_color():
    """name says all"""
    pass

def get_point_extensions(matches, seq):
    """"""
    old_char = ""
    for (start, end) in matches:
        tail = seq[:start]
        match = seq[start:end]
        head = seq[end:]
        match_lowercase = match.lower()
        seq = tail + match_lowercase + head
    char_list = []
    n = 0
    was_upper = False
    for char in seq:
        if char.isupper():
            old_char = char
            char = "."
            was_upper = True
        else:
            char_offset = old_char.lower() if was_upper else ""
            char = char.upper()
            char = char_offset + char
            was_upper = False
        char_list.append(char)
        if n % 10 == 0 and n != 0:
            char_list.append(' ')
        n += 1

    return ''.join(char_list)

def build_coverage_info_from(protein_coverages, dbase):
    """Determines coverage data and parameters.

    prot_mass:      avg protein mass
    matches_number: number of (redundant) peptides matching.
    coverage_aa:    percentage of amino acids covered.
    coverage_peptide:  percentage of total matches to expected tryptic peptides
                    (can be higher than 100%).
    zone_coverage   percentage of the region limited by the first and last
                    matched peptides in a protein sequence
    balance         a measure of how the coverage zone alters de center of
                    gravity of the sequence.
                       0  : equilibrated, same at head than at tail.
                      <0 : zone coverage displaced towards  N-terminal.
                      >0 : zone coverage displaced towards C-terminal.
    prot_view       a string of the protein with dots and amino acid chars.

    """
    total = []
    for key in protein_coverages:
        info = dbase[key]
        protein = info[2]
        prot_mass = get_mass(protein, mode='avg')
        prot_len = len(protein)

        tryptics = digest(protein)
        tryptics = [tryptic for tryptic in tryptics if len(tryptic) > 4]
        tryptic_number = len(tryptics)

        matches = protein_coverages[key]
        matches_number = len(matches)
        matches = refine_matches(matches)
        prot_view = get_point_extensions(matches, protein)

        aas = prot_len - prot_view.count('.')
        coverage_aa = aas * 100. / prot_len
        zone_start = matches[0][0]
        zone_end = matches[-1][1]
        zone = zone_end - zone_start
        zone_coverage = zone * 100. / prot_len
        head_len = prot_len - zone_end
        balance = 100. * (zone_start - head_len) / prot_len
        coverage_peptide = 100. * matches_number / tryptic_number

        data = (key, "%.1f" % prot_mass, "%i" % matches_number,
                     "%i" % coverage_aa, "%i" % coverage_peptide,
                     "%i" % zone_coverage, "%i" % balance, prot_view)

        total.append(data)

    return total

def save_to_xls(total_data, filepath, show=False):
    """Saves list of lists into csv file with .xls extension"""
    heading = ("id\tmass\tnum_peptides\tcoverage_aa\t"
               "coverage_peptide\tzone_coverage\tbalance\tseq\n")

    xls = ['\t'.join(data) for data in total_data]
    excel = heading + '\n'.join(xls)

    try:
        open(filepath, 'w').write(excel)
    except IOError:
        Message('Cannot save %s' % filepath)
    finally:
        if show:
            try:
                os.system(r'start EXCEL.EXE ' + filepath)
            except IOError:
                Message('Cannot open %s' % filepath)

def ask_4_files(test=True):
    """Gets file paths for dbase ans sequence file"""
    if test:
        dbase_path = "C:/Blast/DBs/human_ust.idx"
        file_path = "test/sec_test.txt"
    else:
        message = ("DataBase sqlite(.idx) generated by BLAST program."
                   "Usually in C:/Blast/DBs")
        title = "Select Indexed Sqlite Sequence Database"

        dbase_dir = r"C:"
        file_dir = os.getcwd()

        file_path = AskFileForOpen(windowTitle="Select Sequence File",
                                   defaultLocation=file_dir)

        dbase_path = AskFileForOpen(message,
                                    windowTitle=title,
                                    typeList=["idx"],
                                    defaultLocation=dbase_dir)

    return dbase_path, file_path

def get_coverage(seqs, dbase):
    """Get list of tsv strings with coverage data

    ready to be saved as .csv (xls) file

    """
    protein_coverages = build_protein_coverages_dict(seqs, dbase)
    coverage_info_list = build_coverage_info_from(protein_coverages, dbase)
    return coverage_info_list


if __name__ == '__main__':

    from time import time
    dbase_path, file_path = ask_4_files(test=True)
    basename = os.path.splitext(file_path)[0]

    t0 = time()

    dbase = BaseDataBase(dbase_path)
    seqs = get_sec_list(file_path)
    #
    coverage = get_coverage(seqs, dbase)

    t1 = time()

    save_to_xls(coverage, '%s_%s' % (basename, 'out.xls'))

    print(t1 - t0)

# exWx/setup.py
from distutils.core import setup
# noinspection PyUnresolvedReferences
import py2exe

setup(
    windows=[
            {'script': "coverage.py"
            }
            ],

    options={
            'py2exe': {
                        #'packages' :    [],
                        #'includes':     [],
                        'excludes':     [
                                         'wx','Tkconstants','Tkinter', 'tcl'
                                        ],
                        'ignores':       ['wxmsw26uh_vc.dll'],
                        'dll_excludes': ['libgdk_pixbuf-2.0-0.dll',
                                         'libgdk-win32-2.0-0.dll',
                                         'libgobject-2.0-0.dll'
                                        ],
                        'compressed': 1,
                        'optimize':2,
                        'bundle_files': 1
                        }
            },
    zipfile = None,
    data_files= [
                ]
    )

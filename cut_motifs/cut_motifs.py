#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-


"""
Search in a KimBlast-generated indexed sqlite file (dbase.idx) from a
given sequence database all the sequences in a txt file.
It takes the sequence plus 6 additional amino acids at the head and tail.
Generates an excel files with amino acid frequency data in the N and C-terminal
areas.
Like in Montse thesis to detect enzymatic activities.
"""

import os
from numpy import array
from matplotlib.pyplot import xticks, yticks
from matplotlib.pyplot import title
from matplotlib.pyplot import imshow, savefig

from easygui import fileopenbox as AskFileForOpen
from easygui import msgbox as Message
## ProgressBar
from kimblast.kblast_cls_basedbase import BaseDataBase
#from commons.mass import prob_aa
from commons.mass import prob_aa_brac, prob_aa_vert
from laiamotifs.lmotifs_common import AMINO_ACIDS
from laiamotifs.lmotifs_graph_class import GraphMotif

# '$' is considered here an empty place (No aa)
# it is given an average frequency value
# >>> from commons.mass import prob_aa
# >>> np.mean(prob_aa.values())
# 0.0470
NO_AA = '$'
prob_aa_brac[NO_AA] = 0.05
prob_aa_vert[NO_AA] = 0.05


def get_seq_list(arch):
    """Read the list of sequences from a text file.

    """
    seqs = []
    for line in open(arch, 'r'):
        seqs.append(line.strip())
    return set(seqs)


def fill_dict(seqs, dbase, extend=6):
    """Save by key matched sequences in the database

    Several proteins could have the same sequence tag but with different
    amino acids extending from head and/or tail. Even a protein could
    have the same sequence tag in different areas of its sequence.
    The function is simplified to save only 1 extended sequence per tag
    sequence. That means only the first protein and the first matched
    section of that protein are saved

    return dictionary in the form:
                               tail		     head
    dictionary[prot_ref] = ['LMNDESTRT', 'KSLDSJETETF']
    where   len(head) = extend + n
            where extend = length of sequence before and after the terminal
                           amino acid to be considered
                  n <= extend

    """
    # sometimes MS excel has empty cells which are seen as 'nan' floats
    seqs = [item for item in seqs if not isinstance(item, float)]
    no_aa = NO_AA
    extended_seqs = {}
    maxim = len(dbase)
    # progress = ProgressBar("WAIT, I AM RUNNING %i DBASE ENTRIES" % maxim, maxim)
    for item in dbase:
        protein_seq = item[2]
        remove = []
        prot_len = len(protein_seq)
        for seq in seqs:
            n_start_more = n_end_more = ''
            c_start_more = c_end_more = ''
            start = protein_seq.find(seq)
            if start == -1:
                continue
            end = start + len(seq)

            n_start = start - extend
            if n_start < 0:
                n_start_more = - n_start * no_aa
                n_start = 0

            n_end = start + extend
            if n_end > prot_len:
                n_end_more = (n_end - prot_len) * no_aa
                n_end = prot_len

            c_start = end - extend
            if c_start < 0:
                c_start_more = (- c_start) * no_aa
                c_start = 0

            c_end = end + extend
            if c_end > prot_len:
                c_end_more = (c_end - prot_len) * no_aa
                c_end = prot_len

            tail = n_start_more + protein_seq[n_start:n_end] + n_end_more
            head = c_start_more + protein_seq[c_start:c_end] + c_end_more

            remove.append(seq)
            extended_seqs[seq] = (tail, head)

        for seq in remove:
            seqs.remove(seq)

        # progress.inc()
        #
    # del progress
    return extended_seqs


def initialize_matrices(extended_seqs):
    """"""
    tails = []
    heads = []
    for (tail, head) in extended_seqs.values():
        tails.append(tail)
        heads.append(head)
    return tails, heads


def save_dict():
    """save to work later"""
    pass


def get_sec_with_color():
    """name says all"""
    pass


def calc_freqs(motifs, size):
    """"""
    size *= 2
    aa_number = len(AMINO_ACIDS)
    alist = [0] * size * aa_number
    position = dict()
    for item in AMINO_ACIDS:
        position[item] = AMINO_ACIDS.index(item)

    raw_freqs = array(alist).reshape(aa_number, size)

    for motif in motifs:
        idx = 0
        for amino in motif:
            try:
                pos = position[amino]
            except KeyError:
                print("\n amino %s unknown. Assign to 'X'" % amino)
                pos = position["X"]

            raw_freqs[pos][idx] += 1
            idx += 1

    return raw_freqs


def get_percent_freqs(freqs):
    """Calculates values as percentage of the total"""
    sum_array = freqs.sum(0)
    return freqs.astype(float) * 100 / sum_array


def get_corrected_freqs(freqs, natural_freqs):
    """Correct observed frequencies on the basis of the natural abundance.

    Divides observed frequency by relative abundance.
    Expresses the result as a  percentage.

    """
    prob_list = [[natural_freqs[item]] for item in AMINO_ACIDS]
    prob_aar = array(prob_list)
    corrected_freqs = freqs / prob_aar
    return get_percent_freqs(corrected_freqs)


def save_to_xls(full_data, filepath, show=False):
    """Saves data into csv file with .xls extension"""
    data_list = full_data.tolist()
    xls = []
    for aa, data in zip(AMINO_ACIDS, data_list):
        #print aa, data
        data = [str(item) for item in data]
        text_chunk = '\t'.join(data)
        xls.append(aa + '\t' + text_chunk)

    excel = '\n'.join(xls)

    try:
        open(filepath, 'w').write(excel)
    except IOError:
        Message('Cannot save %s' % filepath)
    finally:
        if show:
            try:
                os.system(r'start EXCEL.EXE ' + filepath)
            except IOError:
                Message('Cannot open %s' % filepath)


def ask_4_files(test=True):
    """Gets file paths for dbase ans sequence file"""
    if test:
        dbase_path = "C:/Blast/DBs/human_ust.idx"
        file_path = "test/sec_test.txt"
    else:
        message = ("DataBase sqlite(.idx) generated by BLAST program."
                   "Usually in C:/Blast/DBs")
        title = "Select Indexed Sqlite Sequence Database"

        dbase_dir = r"C:"
        file_dir = os.getcwd()

        file_path = AskFileForOpen(windowTitle="Select Sequence File",
                                   defaultLocation=file_dir)

        dbase_path = AskFileForOpen(message,
                                    windowTitle=title,
                                    typeList=["idx"],
                                    defaultLocation=dbase_dir)

    return dbase_path, file_path


def get_cut_motifs(seq_list, dbase_path, species='human', extend=6, correct=True):
    """"""
    dbase = BaseDataBase(dbase_path)
    extended_seqs = fill_dict(seq_list, dbase, extend)
    tails, heads = initialize_matrices(extended_seqs)

    if species in ['rat', 'human', 'vertebrates']:
        natural_frequencies = prob_aa_vert
        print('Corrected for Vertebrate frequencies')
    elif species in ['brachyspira']:
        natural_frequencies = prob_aa_brac
        print('Corrected for Brachyspira frequencies')
    else:
        input("Unknown species.\n"
              "Choose between 'vertebrates' and 'brachyspira'\n"
              "Press any key to exit")
        return

    tails_freq = calc_freqs(tails, extend)
    heads_freq = calc_freqs(heads, extend)

    if correct:
        tails_freq = get_corrected_freqs(tails_freq, natural_frequencies)
        heads_freq = get_corrected_freqs(heads_freq, natural_frequencies)

    return tails_freq, heads_freq


def graph_motifs(freqs, header, dir_='', save=True):
    gmotif = GraphMotif(AMINO_ACIDS)
    gmotif.graph(freqs)
    gmotif_name = header
    path = os.path.join(dir_, '%s.png' % gmotif_name)
    imshow(gmotif.img_base)
    title(gmotif_name)
    xticks(range(25, 600, 50), range(1, 13))
    yticks([], [])
    if save:
        savefig(path, dpi=300)


def test_run():
    from time import time
    dbase_path, file_path = ask_4_files(test=True)
    basename = os.path.splitext(file_path)[0]
    seq_list = get_seq_list(file_path)

    t0 = time()
    tails_freq, heads_freq = get_cut_motifs(seq_list, dbase_path)
    save_to_xls(tails_freq, '%s_%s' % (basename, 'tails.xls'))
    save_to_xls(heads_freq, '%s_%s' % (basename, 'heads.xls'))
    t1 = time()
    print(t1 - t0)

    gmotif = GraphMotif(AMINO_ACIDS)
    gmotif.graph(tails_freq)
    gmotif.save('%s_%s' % (basename, 'tails.bmp'))
    gmotif.show()
    #
    gmotif.graph(heads_freq)
    gmotif.save('%s_%s' % (basename, 'heads.bmp'))
    gmotif.show()
    # keep script running for the thread to work
    wait = input('Press any key to continue')

if __name__ == '__main__':
    test_run()

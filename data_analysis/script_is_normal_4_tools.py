#!/usr/bin/env python
import os
import numpy
from scipy.stats import norm, probplot, kstest
from scipy.optimize import curve_fit
from scipy import polyval, random
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import easygui
#
#
def read_data(arch):
    """"""
    datos = []
    for item in open(arch, 'r'):
        item = item.strip()
        if item != '':
            try:
                datos.append(float(item))
            except ValueError:
                pass

    return datos
#
#
def plot_gauss(bins, values, resolution=None, maxim=10, win=None):
    """"""
    A, mu, sigma = curve_fit(gauss, bins, values)[0]
    if sigma < 0:
        sigma = -sigma

    resolution = resolution if resolution else float(maxim)
    lower = int(resolution * (mu - maxim))
    upper = int(resolution * (mu + maxim))

    x = [v / resolution for v in range(lower, upper)]

    data = gauss(x, A, mu, sigma)
    label = r'$\mathrm{Gauss Fit}\ A=%.3f,\ \mu=%.3f,\ \sigma=%.3f$' % (
        A, mu, sigma)
    win.plot(x, data, color='blue', linewidth=2, label=label)
#
#
def plot_probplot(datos, win=None):
    """Use stats.probplot
    scipy.stats.probplot(x, sparams=(), dist="norm", fit=1, plot=None)
    Return (osm, osr)(slope, intercept, r)
    where (osm, osr) are order statistic medians and ordered response data
    respectively so that plot(osm, osr)is a probability plot.
    If fit==1, then do a regression fit and compute the slope (scale),
    intercept (loc), and correlation coefficient (r), of the best straight line
    through the points.
    If fit==0, only (osm, osr) is returned.
    sparams is a tuple of shape parameter arguments for the distribution.

    """
    sort_data = sorted(datos)
    (osm, osr), data = probplot(sort_data, sparams=(),
                                dist='norm', fit=1, plot=None)
    win.plot(osm, osr, 'o')
    # plot title and show
    win.set_title(r'$\mathrm{Probability Plot\ of\ }\ r=%.4f$' % data[2])
    #
    # plot regression line
    x_min, x_max = min(osm), max(osm)
    y_min = polyval(data[0:2], x_min)
    y_max = polyval(data[0:2], x_max)
    win.plot((x_min, x_max), (y_min, y_max))
#
#
def plot_hist(values, win=None):
    """
    """
    (mu, sigma) = norm.fit(values)
    #
    # Kolmogorov-Smirnov
    # p-value= high -> is normal
    centered_values = [item - mu for item in values]
    d, p = kstest(centered_values, 'norm', N=len(values))
    #
    # the histogram of the data
    win.set_title(r'$\mathrm{Histogram}\ KS->D:%.3f, p:%.4f$' % (d, p))
    label = r'$\mathrm{Histogram\ normalized}\ $'
    n, bins, patches = win.hist(values, 60, normed=1, facecolor='green',
                                label=label, alpha=0.75, lw=0.4)
    #
    # plot normal theoretical
    plot_normal(bins, mu, sigma, win)
    #
    # plot gauss
    maxim = max(numpy.abs(bins))
    diff = (bins[1] - bins[0]) / 2
    bins = [bin_ + diff for bin_ in bins[:-1]]

    plot_gauss(bins, n, maxim=maxim, win=win)
#
#
def plot_normal(x, mu, sigma, win=None):
    """"""
    y = mlab.normpdf(x, mu, sigma)
    legend = r'$\mathrm{Normal}\ \mu=%.3f,\ \sigma=%.3f$' % (mu, sigma)
    win.plot(x, y, 'r--', linewidth=1, label=legend)
#
#
def gauss(x, A, mu, sigma):
    """Formula for gauss curve.

    In canonical expression: A = 1 / (sigma * (2 * pi)**0.5)
    """
    return A * numpy.exp(-(x - mu) ** 2 / (2 * sigma ** 2))
#
#
def cut_fraction(series, fraction=100):
    """Remove a <fraction> percent of the extreme values in a series"""
    series = sorted(series)
    total = len(series)
    part = total * fraction / 100
    to_cut = int(total - part)

    end_cut = -1 if to_cut == 0 else -to_cut

    print('eliminated: ', 2 * to_cut)
    return series[to_cut:end_cut]
#
#
def get_datos():
    """Read data from a selected file, test file or generated in situ.
    """
    directory = os.getcwd()
    question = """Select a file. For test, enter: 'normal' or 'real'"""
    archivo = easygui.fileopenbox(question)
    datos = ''

    if archivo is None:
        os.sys.exit()
    elif archivo.endswith(r'\normal'):
        # noinspection PyTypeChecker
        datos = list(random.normal(loc=0, size=10000))
    elif archivo.endswith(r'\real'):
        try:
            archivo = os.path.join(directory, "test\Log(2)_ACRatio.txt")
        except IOError:
            print('not found')
            return
        datos = read_data(archivo)
    else:
        ratios = read_data(archivo)
        datos = cut_fraction(ratios)

    return datos
#
#
def main():
    """Produces four plots for exploratory data analysis.

    See NIST/SEMATECH e-Handbook of Statistical Methods
    http://www.itl.nist.gov/div898/handbook/

    """
    datos = get_datos()
    #
    seq = plt.subplot(2, 2, 1)
    lag = plt.subplot(2, 2, 2)
    his = plt.subplot(2, 2, 3)
    ppn = plt.subplot(2, 2, 4)

    plt.subplots_adjust(left=0.07, bottom=0.07, right=0.95, top=0.94,
                        wspace=None, hspace=0.35)
    #
    # sequence plot
    seq.set_title("Sequence Plot")
    seq.plot(datos)
    #
    # lag plot
    lag.set_title("Lag Plot")
    y_values = datos[1:] + [datos[0]]
    lag.plot(datos, y_values, "o")
    #
    # best fit of data
    plot_hist(datos, win=his)
    #
    # probability plot normal
    plot_probplot(datos, win=ppn)
    #
    his.legend()
    plt.show()
#
#
if __name__ == '__main__':
    #
    main()

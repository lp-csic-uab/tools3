from scipy.stats import norm, kstest
from numpy import linspace
from pylab import hist, show, title, plot
import os

fil = "Log(2)_ACRatio.txt"
DIR = r"test"

arch = DIR + os.sep + fil
values = []

for item in open(arch, 'r'):
    item = item.strip()
    if item != '':
        try:
            values.append(float(item))
        except ValueError:
            pass

range_ = linspace(-10, 10, 1000)
(mu, stdev) = norm.fit(values)
centered_values = [item - mu for item in values]
N = len(values)
rv = norm(loc=mu, scale=stdev)

print("Normal distribution --> mu= %.3f, stdev= %.3f" % (mu, stdev))

form = "Kolmogorov-Smirnov --> D-value %.3f, p-value %.8f"

print(form % kstest(values, 'norm', N=N))
print(form % kstest(centered_values, 'norm', N=N))
print(form % kstest(values, rv.pdf, N=N))
#
curve = norm.pdf(range_, loc=mu, scale=stdev)
hi_norm = max(curve)
hi_dat = max(values)
ratio = hi_dat / hi_norm
curve = [value * ratio for value in curve]

title(fil)
plot(range_, curve, 'r-')
plot(range_, rv.pdf(range_))
a, b, c = hist(values, 50)

show()
##

while True:
        mas = input('More? Y/N = ')
        if mas: break
#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

"""
Define PeptIgnasi and other general tools for handling data from
database extracted peptides.
"""

import pickle
import os
import time
from inspect import ismethod
from fnmatch import fnmatch
from kimpeptid.kpeptid_class import Peptide
from kimblast.kblast_funcs import digest


# noinspection PyMissingConstructor
class PeptIgnasi(Peptide):
    """A simplified subclass of Peptide"""
    def __init__(self, sequence, hydrophobicity=0, retention=0, window=0):
        self.sequence = sequence
        self.hydrophobicity = hydrophobicity
        self.retention = retention
        self.window = window
        self.AH = None
        self.RH = None
        self.RHP = None
        self.LIND = None
        self.sol = None
        self.percent_phobics = None
        self.percent_philics = None
        self.pI = self.calc_pi()
        self.Rt = self.calc_retention()
        self.Q3 = self.calc_q(3)


def change_function(line):
    """A doing nothing function for standardize_heading"""
    return line

def standardize_heading(fasta_path, function=change_function):
    """Transforms the fasta headers as given by function

    function: a function that gets a line of text an returns another line,
              normally a transformed form of the input line

    """
    fasta_data = []
    with open(fasta_path) as f:
        for line in f:
            if line.startswith('>'):
                line = function(line)
            fasta_data.append(line)
    return ''.join(fasta_data)

def extract_dbase(dbase_path, entries):
    """Produces a fasta sub-database containing selected entries

    entries: list of accession numbers to be included in the database

    """
    new_dbase = []
    for part in yield_dbase_part(dbase_path):
        proteins = part.split('>')
        new = [prot for prot in proteins for entry in entries if entry in prot]
        new_dbase += new
    return '>' + '>'.join(new_dbase)
    
def extract_sequences_4_blast(dbase_path, entries, size=100000):
    """Produces a BLAST-adapted fasta sub-database containing selected entries

    entries: list of accession numbers to be included in the database

    """
    new_dbase = []
    for part in yield_dbase_part(dbase_path, size):
        proteins = part.split('>')
        new = [prot for prot in proteins for entry in entries if entry in prot]
        new = [item.replace('\n', '**') for item in new]
        new = [item.replace('**', '\n', 1) for item in new]
        new = [item.replace('**', '') for item in new]
        new = [item + '\n' for item in new] 
        new_dbase += new
    return '>' + '>'.join(new_dbase)

def yield_dbase_part(dbase, size=100000):
    """Yields dbase contents in chunks of size <size>.

    After cutting a section, adds the next characters to finish the last entry
    if required
    
    CAUTION: In Windows, to count correctly the position by read() line endings
    have to be CR/LF. This is nasty

    """
    fhl = open(dbase, 'r')
    while 1:
        txt = fhl.read(size)
        if not txt:
            fhl.close()
            break
        while 1:
            new_char = fhl.read(1)
            if new_char == '>':
                fhl.seek(-1, 1)     # one step back if it finds start tag
                break
            elif new_char == '':
                break
            else:
                txt += new_char

        if txt[0] != '>':
            print('something wrong with database partition')
        yield txt

def build_list_protein_sequences(text, sep='>'):
    """Split a fasta-type text document into blocks of proteins sequences"""
    sequences = []
    paragraphs = text.split(sep)
    for paragraph in paragraphs:
        sequence = ''.join(paragraph.split('\n')[1:])
        sequences.append(sequence)

    return sequences

def get_peptide_instances_from_dbase(dbase, chunk_size):
    """Produces PeptIgnasi instances from the contents of a fasta dbase.

    """
    for part in yield_dbase_part(dbase, chunk_size):
        sequences = build_list_protein_sequences(part)
        number_of_proteins = len(sequences)
        yield (build_peptide_instances_list_from_proteins(sequences),
               number_of_proteins)

def build_peptide_instances_list_from_proteins(sequences, long_min=4):
    """Produces a list of PeptIgnasi instances from a list of protein sequences.

    """
    peptides = []
    for sequence in sequences:
        print('-', end=' ')
        for tryptic in digest(sequence, 'trypsin'):
            if len(tryptic) >= long_min:
                peptides.append(PeptIgnasi(tryptic))
    return peptides

def build_peptide_instances_list_from_peptides(sequences, long_min=7):
    """Produces a list of PeptIgnasi instances from a list of peptide sequences.

    """
    peptide_instances = []
    for sequence in sequences:
        print('-', end=' ')
        if len(sequence) >= long_min:
            peptide_instances.append(PeptIgnasi(sequence))
    return peptide_instances

def save_peptide_instances(peptide_instances, file_path, params=('Q3', 'pI')):
    """"""
    lines = []
    number_attributes = len(params)
    form = '%s' + '\t%.3f' * number_attributes

    for peptide in peptide_instances:
        line_items = ()
        for param in params:
            func = getattr(peptide, param)
            if ismethod(func):
                func = func()
            line_items += tuple((func,))
        line_items = (peptide.sequence,) + line_items

        line = form % line_items
        lines.append(line)
    text = '\n'.join(lines) + '\n'
    open(file_path, 'a').write(text)

def build_pickle_file(the_object, filepath, number=1):
    """Serializes the_object as a pickle file.

    Pickle file has a name: fullname_number

    """
    chunk_save_path = filepath + '_' + str(number)
    # Pickle in binary (type 2) format.
    # Needs the file to be open as binary (wb)
    pickle.dump(the_object, open(chunk_save_path, 'wb'), 2)
    return chunk_save_path

def extract_from_pickle_files(pickle_files, param):
    """Reads the value of a param for all instances serialized in pickle_files.

    returns list of values

    """
    attributes = []
    for file_path in pickle_files:
        peptide_chunk = pickle.load(open(file_path, 'rb'))
        for peptide in peptide_chunk:
            attributes.append(getattr(peptide, param))
    return attributes

def extract_column_from_tsv(tsv_path, column=1):
    """Extract the values in a column from a tsv file

    Used for example to make a plot or histogram of the values

    """
    values = []
    for line in open(tsv_path):
        value = float(line.split('\t')[column])
        values.append(value)
    return values


def build_pickle_file_list(target_filename, dir_="C:/"):
    """Search for pickle files with name starting as the one given.

    Checks identity on four first chars.

    """
    pickle_files = []
    for filename in os.listdir(dir_):
        if fnmatch(filename, target_filename[:4] + '*.pckl_*'):
            pickle_files.append(dir_ + filename)
    return pickle_files

def batch_extract_from_dbase(dbase_name, tsv_path=None, *params):
    """Calculates params of sequences in dbase.

    tsv_path: if given, data is saved in tsv file.

    builds series of pickle files with PeptideIgnasi instances.
    returns dictionary of param values for all sequences.

    """
    dictionary = {}
    for param in params:
        dictionary[param] = []
    #
    open(tsv_path, 'w').write('')
    pickle_files = build_pickle_file_list(dbase_name)
    #
    for pick in pickle_files:
        peptides = cPickle.load(open(pick, 'rb'))
        text = ''
        for peptide in peptides:
            sequence = peptide.sequence
            param_text = ''
            for param in params:
                param_value = getattr(peptide, param)
                if ismethod(param_value):
                    param_value = param_value()
                dictionary[param].append(param_value)
                param_text += '\t%.2f' % param_value
            #
            text += sequence + param_text + '\n'

        if tsv_path:
            open(tsv_path, 'a').write(text)
    return dictionary

def do_hist(param_dict, *params):
    """Plot an histogram for each parameter in params"""
    from pylab import hist, show, hold
    hold(True)
    for param in params:
        hist(param_dict[param], 200)
    show()


if __name__ == "__main__":
    #
    from pylab import hist, show
    import os
    ###########3
    DIR = r"test"
    DIR_OUT = DIR + os.sep + "out"
    #
    dbase_name = r"tefa_reduced.fasta"
    name, ext = os.path.splitext(dbase_name)
    #
    dbase_path = DIR + os.sep + dbase_name
    pickle_path = DIR_OUT + os.sep + name + '.pckl'
    tsv_path = DIR_OUT + os.sep + name + '_text.txt'
    xls_path = DIR_OUT + os.sep + name + '_datos.xls'
    chunk_size = 1000000
    #############
    #
    TIME1 = time.time()
    #
    protein_number = 0
    peptide_number = 0
    pickles = []
    count = 1
    params = ('Q3', 'pI')
    open(tsv_path, 'w').write('')

    for peptides_chunk, protein_count in get_peptide_instances_from_dbase(
            dbase_path, chunk_size):
        peptide_number += len(peptides_chunk)
        protein_number += protein_count
        save_peptide_instances(peptides_chunk, tsv_path, params)
        pickle = build_pickle_file(peptides_chunk, pickle_path, count)
        pickles.append(pickle)
        sequences = []
        peptides_chunk = []
        count += 1
    #
    TIME2 = time.time()
    print('total_proteins= ', protein_number)
    print('total_peptides= ', peptide_number)
    print('time = ', TIME2 - TIME1)
    #
    charges = extract_from_pickle_files(pickles, 'Q3')
    hist(charges, 400)
    show()
    #
    pis = extract_column_from_tsv(tsv_path, 2)
    hist(pis, 400)
    show()
    #
    while True:
        more = input('More? Y/N = ')
        if more:
            break

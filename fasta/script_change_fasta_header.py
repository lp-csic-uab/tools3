"""
creates a fasta database with the header modified
this was created on sept-2013 to convert the microbe patogen database VFDB to
the uniprot format in order for it to be blast-formatted for KimBlast.
"""

__author__ = 'joaquin'

import os
import dbase_tools as dbt

function = lambda x: x[0] + 'n' + x[1:]

def from_microbe_to_uniprot(line):
    """
    typical uniprot
    >sp|Q4U9M9|104K_THEAN 104 kDa micro/rhoptry prec - Theileria an...
    microbe in 2014
    >VFG0676 (gi:4894323) lef - anthrax toxin lethal factor precursor, lef, [Bacillus anthracis Sterne] (VF0142)
    will be changed to:
    >gi|4894323|VFG0676  lef - anthrax toxin lethal factor precursor, lef, [Bacillus anthracis Sterne] (VF0142)
    """
    head, tail = line.split(')', 1)
    vfg, id = head.split(' (')
    vfg = vfg[1:]
    id = id[3:]
    newline = '>gi|%s|%s%s' %(id, vfg, tail)
    return newline
    
def from_microbe_to_uniprot_2016(line):
    """
    typical uniprot
    >sp|Q4U9M9|104K_THEAN 104 kDa micro/rhoptry prec - Theileria an...
    microbe in 2016
    >VFG000677(gi:4894326) (pagA) anthrax toxin moiety, protective antigen [Anthrax toxin (VF0142)] [Bacillus anthracis str. Sterne]
    will be changed to:
    >gi|4894323|VFG000677  (pagA) anthrax toxin moiety, protective antigen [Anthrax toxin (VF0142)] [Bacillus anthracis str. Sterne]
    """
    head, tail = line.split(')', 1)
    vfg, id = head.split('(')
    vfg = vfg[1:]
    id = id[3:]
    newline = '>gi|%s|%s%s' %(id, vfg, tail)
    return newline    


if __name__ == "__main__":
    
    directory = os.path.expanduser('~/Desktop')
    input_dbase = "VFs.faa"

    input_path = os.path.join(directory, input_dbase)
    output_path = os.path.join(directory, input_dbase + '.out')

    txt = dbt.standardize_heading(input_path, from_microbe_to_uniprot)
    with open(output_path, 'w') as f:
        f.write(txt)



#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

"""
creates a fasta database with references in list
"""

import dbase_tools as db


dbase = 'C:/Users/joaquin/omssa214/rev_Human_Swissprot_2011_04.fasta'
out = 'C:/Users/joaquin/omssa214/rev_Human_4_test.fasta'
refs = ['decoy_14939', 'decoy_15966', 'decoy_16022', 'decoy_16029',
        'decoy_23442', 'decoy_2378', 'decoy_24423', 'decoy_24968',
        'decoy_26149', 'decoy_27405', 'decoy_33372', 'decoy_7545', 'decoy_8500',
        'P00450', 'P01008', 'P01024', 'P01042', 'P02647', 'P02649', 'P02671',
        'P02675', 'P02746', 'P02751-10', 'P02753', 'P02763', 'P02765',
        'P04196', 'P05155', 'P05156', 'P06396-2', 'P06727', 'P43652', 'P69905',
        'P78363', 'Q02985', 'Q13049', 'Q15291', 'Q6IQ49-3', 'Q9HC96',
        'Q9NZM3-2']

txt = db.extract_dbase(dbase, refs)

with open(out, 'w') as f:
    f.write(txt)
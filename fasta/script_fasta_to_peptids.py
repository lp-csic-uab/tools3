#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
#
"""
Joaquin Abian, March 2008

From a fasta dbase or a text file build pickle and text files containing
sequences and associated parameters.

The pickle file contains instances of class PeptIgnasi
The text file contains a list of sequences and some selected parameters
  do_txt_archive(collection_partial, txt_path, ('Q3', 'pI', 'Rt'))
The program divides the pickle files into chunks of <partial_size> size to
 prevent memory problems.
"""


from dbase_tools import extract_from_pickle_files
from dbase_tools import save_peptide_instances, build_pickle_file
from dbase_tools import extract_column_from_tsv
from dbase_tools import get_peptide_instances_from_dbase
from dbase_tools import build_peptide_instances_list_from_peptides


def run_fasta(dbase, chunk_size, text_path, save_path, params):
    """Builds pickle and text files containing sequences parameters.

    dbase: fasta database of protein/peptide sequences.

    """
    pickles = []
    file_number = 0
    total_protein_number = 0
    peptide_number = 0
    for peptides_chunk, protein_number in get_peptide_instances_from_dbase(
            dbase, chunk_size):
        total_protein_number += protein_number
        peptide_number += len(peptides_chunk)
        save_peptide_instances(peptides_chunk, text_path, params)
        pick = build_pickle_file(peptides_chunk, save_path, file_number)
        pickles.append(pick)
        file_number += 1

    return pickles, total_protein_number, peptide_number

def run_text(dbase, text_path, save_path):
    """Builds pickle and text files containing sequences parameters.

    dbase: text file of sequences

    """
    pickles = []
    peptide_number = 0
    sequences = [item.strip() for item in open(dbase, 'r')]
    #
    lengths = [len(item) for item in sequences]
    num_prot = len(sequences)
    peptides = build_peptide_instances_list_from_peptides(sequences)
    peptide_number += len(peptides)
    save_peptide_instances(peptides, text_path)
    pick = build_pickle_file(peptides, save_path)
    pickles.append(pick)

    return pickles, num_prot, peptide_number, lengths


if __name__ == '__main__':

    import time
    import os
    from pylab import hist, show, title
    #
    # ------------      To Modify     ------------- #
    DIR = 'test'                                    #
    dbase_name = 'fish_db_reduced.txt'              #
    type_ = ''                  # fasta/txt         #
    # --------------------------------------------- #
    #
    if not all((DIR, dbase_name, type_)):
        DIR = 'test'
        DIR_OUT = DIR + os.sep + 'out'
        dbase_name = "fish_db_reduced.txt"
        type_ = 'fasta'
    else:
        DIR_OUT = DIR
    #
    name, ext = os.path.splitext(dbase_name)
    #
    dbase = DIR + os.sep + dbase_name
    save_path = DIR_OUT + os.sep + name + '.pckl'
    txt_path = DIR_OUT + os.sep + name + '_text.txt'
    xls_path = DIR_OUT + os.sep + name + '_datos.xls'
    chunk_size = 1000000
    #############

    TIME1 = time.time()
    #
    open(txt_path, 'w').write('')
    #
    length = None
    if type_ == 'fasta':
        attributes = ('Q3', 'pI')
        pickles, protein_number, peptide_number =\
            run_fasta(dbase, chunk_size, txt_path, save_path, attributes)
    # if type_ == 'txt':
    else:
        pickles, protein_number, peptide_number, length =\
            run_text(dbase, txt_path, save_path)

    TIME2 = time.time()
    print 'total_proteins = ', protein_number
    print 'total_peptides = ', peptide_number
    print 'time = ', TIME2 - TIME1
    #
    if length:
        values = hist(length, 150)
        title('Distribution peptide lengths')
        show()
        #
        raw_input('Press key to continue')
    #
    charges = extract_from_pickle_files(pickles, 'Q3')
    values = hist(charges, 400)
    title('Distribution Charge at pH 3')
    show()
    #
    raw_input('Press key to continue')

    pi_list = extract_column_from_tsv(txt_path, 2)
    values = hist(pi_list, 400)
    title('pI Distribution')
    show()
    #
    raw_input('Press key to finish')
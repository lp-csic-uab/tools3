#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

"""
JAbian Oct 2011
Extracts normal and reversed (decoy) sections from a reverse database.
The database must have the decoy section after the normal one and with the tag
">decoy".
"""

import os

direct = os.path.expanduser("~/Escritorio")
arch = "rev_Human_Swissprot_2011_04.fasta"
rev = "Human_Swissprot_2011_04_reverse.fasta"
norm = "Human_Swissprot_2011_04.fasta"

join = os.path.join

arch = join(direct, arch)
rev = join(direct, rev)
norm = join(direct, norm)

with open(arch) as fin:
    with open(rev, 'w') as f_rev:
        with open(norm, 'w') as f_norm:
            txt = fin.read()
            index = txt.find('>decoy')
            f_norm.write(txt[:index])
            f_rev.write(txt[index:])

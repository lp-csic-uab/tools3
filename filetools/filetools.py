#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

"""
Define tools for file and directory handling.
"""

import os
import shutil


def remove_files_with_extensions(dir_path, extensions):
    """Remove files in folder <dir_path> if its type is included in <extensions> list."""
    for dp, dn, fls in os.walk(dir_path):
        for fl in fls:
            for extension in extensions:
                if fl.endswith(extension):
                    print fl
                    os.remove(os.path.join(dp, fl))

def copy_trees(folders, dir_root, dir_target, ignore):
    """Copy the contents of the folders in <folders> list into a directory, ignoring files of types in <ignore> list"""
    for folder in folders:
        source = os.path.join(dir_root, folder)
        dest = os.path.join(dir_target, folder)
        shutil.copytree(source, dest, ignore=shutil.ignore_patterns(*ignore))
#!/usr/bin/env python
# -*- coding: latin-1 -*-
#
"""
#Funciones para Gas Phase Fractionation
#junio 2008
"""
#
#
import matplotlib.pyplot as plt
from plot_spectro import subtract_baseline, std_filter
from collections import defaultdict
from array import array
import os
import base64
import EasyDialogs
#
def read_base64_spectrum(xml_line):
    """De-codifies line with base64 encoded spectrum from mzXML file.
     """
    xml_line = xml_line.split('>')[1]
    xml_line = xml_line.split('<')[0]

    spectrum = array('f')
    spectrum.fromstring(base64.b64decode(xml_line))
    spectrum.byteswap()
    return spectrum

def format_spectrum(spectrum, cutoff):
    """Converts spectrum from linear to list of pairs mass, intensity.

    [mz1, int1, mz2, int2, ...] --> [(mz1,int1),(mz2,int2),(mz3,int3),...]

    """
    formatted = []
    i = 0
    while True:
        try:
            intensity = spectrum[i + 1]
        except IndexError:
            break
        if intensity > cutoff:
            formatted.append((spectrum[i], intensity))
        i += 2

    return formatted

def get_spectra_from_mzXML(filepath, cutoff=0):
    """"""
    spectra = []
    it_comes_next = False
    for line in open(filepath):
        line = line.strip()
        if not it_comes_next:
            if line.startswith("<scan num"):
                new_scan = line
            elif line.startswith('msLevel="1"'):
                it_comes_next = True
        else:
            if line.startswith("retention"):
                rt = line.split("PT")[1][:-2]
                rt = float(rt)
            elif line.startswith("pairOrder"):
                it_comes_next = False
                spectrum_string = read_base64_spectrum(line)
                spectrum = format_spectrum(spectrum_string, cutoff)
                # noinspection PyUnboundLocalVariable
                spectra.append((new_scan, rt, spectrum))
    return spectra

def plot_spectrum(spectrum, n=0):
    """"""
    plt.figure(n, figsize=(8, 8))
    plt.axes([0.1, 0.1, 0.8, 0.8])

    xa = [x for x, y in spectrum]
    ya = [y for x, y in spectrum]

    plt.bar(xa, ya, width=0.3)

def plot_gpf_map(gpf_maps, gpf_sectors, archivo, n=0, save=False):
    """Draws gpf map and saves figure"""
    colors = ['lightblue', 'lightgreen', 'yellow', 'orange', 'red', 'black']
    f = plt.figure(n, figsize=(8, 8))
    plt.axes([0.1, 0.1, 0.8, 0.8])
    plt.hold(True)
    #
    for group, color in zip(gpf_maps, colors):
        plt.plot(group[0], group[1], marker='.', color=color, linestyle='None')

    plt.title(archivo)
    #
    fverticalx1 = []
    verticaly = [400, 2000]
    for rt_start, rt_end, mass_range in gpf_sectors:
        fverticalx1.append(rt_start)           # should be adjusted per case
        horizontalx = [rt_start, rt_end]
        fhorizontaly = []
        for mass in mass_range:
            fhorizontaly.append(mass)
        #
        plt.plot(horizontalx, [fhorizontaly, fhorizontaly], 'b-')
    #
    plt.plot([fverticalx1, fverticalx1], verticaly, 'b-')
    #
    if save:
        name = archivo[:-6] + ".png"
        plt.savefig(name, format='png')

    return f

def build_gpf_map(spectra, levels=None):
    """Build gas phase fractionation map data for plotting

    spectra: list of pairs (rt, spectrum) where
             spectrum: [(mz, intensity), .....]
    levels: intensity levels

    """
    if not levels:
        levels = [5000, 10000, 50000, 200000, 1000000, 1000000000]
    low_limit = 2000
    ranges = []
    gpf_map = []
    #
    for new_limit in levels:
        ranges.append((low_limit, new_limit))
        low_limit = new_limit
    #
    for range_ in ranges:
        x_map_values = []
        y_map_values = []
        for rt, spectrum in spectra:

            y_values = [v[0] for v in spectrum if range_[1] > v[1] > range_[0]]
            x = [rt] * len(y_values)
            x_map_values.extend(x)
            y_map_values.extend(y_values)

        gpf_map.append((x_map_values, y_map_values))

    return gpf_map

def get_sectors(spectra, sectors=11):
    """Build time sectors and mass sections of a LC-MS spectrum.

    Divides chromatogram time scale in <sectors> sectors and uses
    <get_mass_sections> to determine the mass sections in each sector.

    spectra: list of pairs (rt, spectrum) where spectrum is a list of mass,
     intensity values

    Returns column ranges for plotting :
       [(rt-start, rt-end, [mass-1, ..., mass-n]), ...., ()]
    p.e.
     [(10.1, 25.5, [400.5, 444.0, 535.5, 607.0, 812.5]),
      (25.5, 40.9, [401.0, 586.0, 762.0, 933.0, 1276.5]),
        ..............................................
      (164.4, 179.9, [400.0, 652.0, 917.5, 1142.5, 1927.0])]

    """
    rt_start = spectra[0][0]
    spectrum_number = len(spectra)
    rt_end = spectra[spectrum_number - 1][0]
    width = (rt_end - rt_start) / sectors
    column_ranges = []
    for sector in range(sectors):
        end = rt_start + width
        column = [item[1] for item in spectra if rt_start < item[0] < end]
        mass_ranges = get_mass_sections(column)
        column_ranges.append((rt_start, end, mass_ranges))
        rt_start = end

    return column_ranges

def get_mass_sections(column, section_number=4, min_peaks=1000):
    """Determines mass ranges containing same number of ions.

    If number of peaks per section is lower than <min_peaks>, intensity cutoff
    is reduced to increase the number.
    m/z values are adjusted to 0.5 mass units

    """
    counts = defaultdict(int)
    ions_number = 0
    cutoff = 100000
    #
    while True:
        for spectrum in column:
            for mz, intensity in spectrum:
                if intensity > cutoff:
                    mz = '%.1f' % mz
                    decimal = int(mz[-1])
                    if decimal > 4:
                        mz = mz[:-1] + '5'
                    else:
                        mz = mz[:-1]

                    counts[mz] += 1

        ions_number = len(counts)
        if ions_number > min_peaks:
            break
        else:
            cutoff *= 0.5
            if cutoff < 100:
                break

    alto = ions_number / section_number
    masses = [float(mass) for mass in counts]
    masses.sort()

    mass_sections = []
    for i in range(section_number):
        mass_sections.append(masses[i * alto])

    mass_sections.append(masses[-1])
    return mass_sections
#
def is_batch():
    """"""
    question = """Process all mzXML files in directory ?
If 'NO', you should choose a single file"""
    answer = EasyDialogs.AskYesNoCancel(question)
    if answer == -1:
        os.sys.exit()
    return True if answer else False
#
def get_arch_names_to_process(search_dir):
    """Gets the list of mzXML archives in a selected <search_dir> directory"""
    mzXML_file_paths = []
    if is_batch():
        root_dir = EasyDialogs.AskFolder(defaultLocation=search_dir)
        for name in os.listdir(root_dir):
            if name.endswith("mzXML"):
                name = os.path.join(root_dir, name)
                mzXML_file_paths.append(name)
    else:
        work_file = EasyDialogs.AskFileForOpen()
        mzXML_file_paths = [work_file]
    return mzXML_file_paths
#
#
if __name__ == "__main__":

    from time import time
    #
    current_dir = os.getcwd()
    #
    test_dir = r"test"
    test_1 = r"E6_GluC_0mM.mzXML"
    test_files = [test_1]
    #
    DO_TEST = True
    #
    if DO_TEST:
        file_paths = [os.path.join(current_dir, test_dir, filename)
                      for filename in test_files]
    else:
        file_paths = get_arch_names_to_process(current_dir)

    #
    time_0 = time_1 = time_2 = time_3 = time()
    figures = []
    for n, filepath in enumerate(file_paths):
        filename = os.path.basename(filepath)
        raw_spectra = get_spectra_from_mzXML(filepath, cutoff=2000)

        time_1 = time()
        spectra = [(tm / 60, std_filter(subtract_baseline(sp)))
                    for sc, tm, sp in raw_spectra if sp]

        time_2 = time()
        gpf_sectors = get_sectors(spectra)
        gpf_map = build_gpf_map(spectra)

        time_3 = time()
        f = plot_gpf_map(gpf_map, gpf_sectors, filename, n, save=True)
        figures.append(f)

    time_4 = time()                            # seconds
    print 'read     %.1f' % (time_1 - time_0)  # 2.5
    print 'spectrum %.1f' % (time_2 - time_1)  # 46
    print 'build    %.1f' % (time_3 - time_2)  # 3.7
    print 'plot     %.1f' % (time_4 - time_3)  # 4.2

    plt.show()
    raw_input('enter for close')

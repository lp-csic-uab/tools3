import os
import scipy

def baselinear(x_values, y_values, resolution=100, sectors=40, degree=5):
    """"""
    y_values = list(y_values)
    xx, yy = get_min(x_values, y_values, resolution)
    xx, yy = correct_min(xx, yy, y_values)
    xx, yy = correct_min(xx, yy, y_values)
    #
    curve = adjust_curve(xx, yy, sectors, degree)
    y_values = subtract_curves(y_values, curve)
    return zip(x_values, y_values)

# noinspection PyUnusedLocal
def get_min(x_values, y_values, resolution=20):
    """Get y minimums of a spectrum in ranges given by <resolution>

    Includes start and end points

    """
    y_values = scipy.array(y_values)
    ymins = [y_values[0]]
    xmins = [0]
    len_y_values = y_values.size
    resolution = min(resolution, len_y_values / 3)
    range_ = scipy.linspace(0, len_y_values, resolution)
    range_ = range_.astype(int)
    for i in range(resolution - 1):
        y_sec = y_values[range_[i]:range_[i + 1]]
        minim = y_sec.min()
        index = y_sec.argmin() + range_[i]
        ymins.append(minim)
        xmins.append(index)          # should be append(x_values[index])
    xmins.append(len_y_values)       # the end point
    ymins.append(y_values[-1])
    return xmins, ymins              # list

def correct_min(xmins, ymins, y_data):
    """ Do linear adjusts from the given minimums.

    xmins: [list of x_minimums]
    ymins: [list of y_minimums]
    y_data: [spectrum]

    For each adjust checks for and modifies values under the curve.

    """
    #
    #ymin=[max(0,item) for item in ymin]
    total_min = len(ymins)
    x_values = []
    y_values = []
    for minimum in xrange(total_min - 1):
        xpair = xmins[minimum:minimum + 2]
        ypair = ymins[minimum:minimum + 2]
        x_range = xrange(xpair[0], xpair[1])

        f = scipy.polyfit(xpair, ypair, 1)        # linear adjust
        val = scipy.polyval(f, x_range)           # could jump more
        difference = y_data[xpair[0]:xpair[1]] - val
        negatives = [value for value in difference if value < 0]
        if len(negatives) > (len(difference) / 100):
            new_xmin = difference.argmin() + xpair[0]
            if new_xmin not in xmins:
                new_ymin = y_data[new_xmin]
                #
                x_values.append(new_xmin)
                y_values.append(new_ymin)
    if len(x_values) > 0:
        for idx in xrange(len(x_values) - 1, -1, -1):
            xmins.append(x_values[idx])
            xmins.sort()
            x_min_index = xmins.index(x_values[idx])
            ymins.insert(x_min_index, y_values[idx])

    return xmins, ymins       # list

def adjust_curve(xmins, ymins, sectors=5, degree=5, excess=100):
    """Fit curve to xmins, ymins series

     1.-adjust a polynomial of degree <degree> in <sectors> sectors.
     2.-calculates polynomial values for each x_mins value.

     these values will by subtracted from the original spectrum.

    xmins, ymins: zonal minimums from a spectrum
    excess: extension of points per step (to smooth interface between sectors)

    """
    length = xmins[-1]
    points_per_sector = length / sectors

    points = zip(xmins, ymins)
    min_ini = 0
    y_fitted = []
    is_first_sector = True
    shift_start = 0
    for sector in range(sectors):
        ini = min_ini - shift_start
        end = min_ini + points_per_sector + excess
        sector_points = [point for point in points if ini <= point[0] < end]
        #
        x_values = [point[0] for point in sector_points]
        y_values = [int(point[1]) for point in sector_points]
        #
        if is_first_sector:
            shift_start = excess     # for next sector...
            is_first_sector = False
        #
        try:
            f = scipy.polyfit(x_values, y_values, degree)
        except ValueError:
            print 'error'
            return None, None
        #
        x_series = range(min_ini, min_ini + points_per_sector)
        sector_y_fitted = list(scipy.polyval(f, x_series))
        y_fitted.extend(sector_y_fitted)
        min_ini += points_per_sector

    return y_fitted    # list

def subtract_curves(original, other):
    """

    subtracts only positive values

    """
    subtrahend = scipy.array(other)
    subtrahend = scipy.maximum(0, subtrahend)
    minuend = scipy.array(original)
    # noinspection PyTypeChecker
    length = min(len(minuend), len(subtrahend))
    return minuend[:length] - subtrahend[:length]

def get_spectrum_at(spectra, rt):
    """"""
    for spectrum in spectra:
        if abs(spectrum[0] - rt) < 0.1:
            return spectrum[1]

def fill_gaps(spectrum, max_gap=2):
    """"""
    filled = []
    try:
        x_old = spectrum[0][0]
    except IndexError:
        x_old = 0
    for x, y in spectrum:
        gap = x - x_old
        if gap > max_gap:
            dist = 1.1 * gap / max_gap
            x_old += dist
            while x_old < x:
                filled.append((x_old, 0))
                x_old += dist

        filled.append((x, y))
        x_old = x

    return filled

def subtract_baseline(spectrum):
    """"""
    if not spectrum:
        return []
    extension_right = 20
    spectrum = fill_gaps(spectrum)

    x_values = [x for x, y in spectrum]
    y_values = [y for x, y in spectrum]

    x_last = int(x_values[-1])
    x_extension = range(x_last + 1, x_last + 1 + extension_right)
    x_values.extend(x_extension)
    y_values.extend([0] * extension_right)
    ms = baselinear(x_values, y_values)
    msp = [(x, max(0, y)) for x, y in ms]
    return msp[0:-extension_right]

def std_filter(spectrum):
    """"""
    if not spectrum:
        return []
    filtered = []
    spectrum = fill_gaps(spectrum)
    y_values = [y for x, y in spectrum]
    #
    resolution = 10
    length_y_values = len(y_values)
    rang = scipy.linspace(0, length_y_values, resolution)
    rang = rang.astype(int)
    #
    for i in range(resolution - 1):
        y_sector = y_values[rang[i]:rang[i + 1]]
        spectrum_range = spectrum[rang[i]:rang[i + 1]]
        y_sector = scipy.array(y_sector)
        sector = scipy.sqrt(y_sector)
        mean = sector.mean()

        nsector = [point for point, y_value in zip(spectrum_range, sector)
                   if y_value > mean]
        filtered.extend(nsector)

    return filtered


if __name__ == "__main__":

    from time import time
    from matplotlib import pyplot as plt
    from gpf_funcs import plot_spectrum, get_spectra_from_mzXML
    dir_ = os.path.expanduser(r"~\Escritorio\ReAdW-3.5.4\RAWfiles_test")
    dir_ = 'test'
    filename = r"E6_GluC_0mM.mzXML"

    rt = 86.44

    filepath = dir_ + os.sep + filename
    spectra_raw = get_spectra_from_mzXML(filepath, cutoff=0)
    spectra = [(tm / 60, sp) for sc, tm, sp in spectra_raw]

    spectrum = get_spectrum_at(spectra, rt)
    f1 = plot_spectrum(spectrum, 0)
    #
    time_0 = time()
    #
    spectrum_flat = subtract_baseline(spectrum)
    f2 = plot_spectrum(spectrum_flat, 1)
    #
    spectrum_filtered = std_filter(spectrum_flat)
    time_1 = time()
    f3 = plot_spectrum(spectrum_filtered, 2)
                                            # vs 01      02      03
    print 'time  %.3f' % (time_1 - time_0)  # 0.411 -> 0.360 ->0.361

    plt.show()
    raw_input('something for end')

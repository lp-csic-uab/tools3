#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

"""
Extract non-enzymatically produced peptides from collections of identifications
from trypsin or gluc digest of proq
"""

__author__ = 'Joaquin'

import pandas as p
import os
import time
from _tools.pantools.martin import get_no_gluc, get_no_tryptic
from _tools.pantools.martin import remove_ptms


def read_from_csv(input_file):
    """Reads a tsv file into a pandas dataframe.
    """
    return p.read_csv(input_file, sep='\t', index_col=False)


def get_no_enzymatic(df, mode='trypsin'):
    """Extract no enzymatically generated sequences in a dataframe df

    Input dataframe is one generated from an omssa-derived excel.
    Returns a dataframe with selected columns and some calculations performed:
      - col 'pos': position of the aa (carboxy side) involved in the cut
      - col 'aa': the aa involved in the cut
      - col 'side': side of the peptide where the no enzymatic cut is detected
                    (N-term o C-term)

    """
    func = {'trypsin': get_no_tryptic, 'gluc': get_no_gluc}
    df = df[df['Accession'] == 0]
    df['Peptide'] = df['Peptide'].apply(remove_ptms)
    df['side'], df['aa'], df['pos'] = zip(*df['Peptide'].map(func[mode]))
    df = df.fillna(0)
    df = df[df['aa'] != 0]
    return df[['scan 1', 'Peptide', 'Start', 'Stop', 'side', 'aa', 'pos']]


def get_counts_for(df):
    """Determines the counts of an item in a dataframe column

    returns the dataframe with each repeated item removed except for one and
    with a new column indicating the number of repetitions found.

    The row kept is the one given by drop_duplicates.
    Information on other columns of the removed rows is lost

    """
    repetitions = df.groupby('Peptide').size()
    df = df.drop_duplicates('Peptide')
    df['counts'] = df['Peptide'].map(repetitions)
    return df


def build_readme(d):
    """Build a dataframe with the info in a dictionary d
    """
    readme = p.Series(d, name='Parameters used')
    return p.DataFrame(readme)


def save_as_excel(out_path, readme, dfs, decimals=1):
    """Save a group of dataframes, including a readme, into and excel file.

    dfs: dictionary of dataframes.
         keys will be the sheet names in the excel file.

    """
    decimals = '%' + '.%if' % decimals 
    writer = p.ExcelWriter(out_path)
    readme.to_excel(writer, sheet_name='README')
    for item, df in dfs.iteritems():
        df.to_excel(writer, sheet_name=item, float_format=decimals)
    writer.save()


def get_band(x):
    """Returns the band name from a Leti file name

    file names are of the type:
      - 2012_328_LET_4_B_8_9_2007_xug_LCMSMS_210812_04
      - 2012_328_LET_4_B_13a_2007_xug_LCMSMS_210812_04
    returns: 8 and 13a, respectively (TODO: 8 should be 8_9)

    """

    names = x.split('_')
    return names[5]


def in_single(df, name, mode):
    """Search no enzymatically generated peptides from a single file"""
    df = get_no_enzymatic(df, mode)
    return {name: get_counts_for(df)}


def in_batch(df, bands, mode):
    """Search no enzymatically generated peptides in an df build from several omssa files.

    This is a very specific function.
    Used for dfs containing omssa info from several files each corresponding to a gel band.
    files are of the type:
      - 2012_328_LET_4_B_8_9_2007_xug_LCMSMS_210812_04
    and <bands> contains a list of bands which are searched in this file name (p.e. '8_9').

    """
    d = {}
    df = get_no_enzymatic(df, mode)
    df['lane'] = df['File'].apply(get_band)

    for band in bands:
        bdf = df[df['lane'] == band]
        bdf = bdf[['scan 1', 'Peptide', 'Start', 'Stop', 'side', 'aa', 'pos']]
        d[band] = get_counts_for(bdf)

    return d


if __name__ == "__main__":
    dir_ = os.path.expanduser(
        '~/nbooks_data/proq/peptides/ProQ_crude_fragments')
    f = '2012_328_Part5_TGold_Omssa_NoEnzyme.xls'
    f = '2012_328_Part5_G_Omssa_NoEnzyme.xls'

    dir_ = os.path.expanduser(
        '~/nbooks_data/proq/peptides/ProQ_bands_fragments')
    f = '2012_328_Part4B1_Omssa_NoEnzyme.xls'
    bands = ['1', '5', '6', '7', '8', '10']

    name = os.path.splitext(f)[0]
    f = os.path.join(dir_, f)
    fout = os.path.join(dir_, '%s_out.xlsx' % name)

    df = read_from_csv(f)

    d = in_batch(df, bands, mode='trypsin')
    d = in_single(df, 'no_gluc', mode='gluc')

    readme = build_readme(
        {'WHAT': 'Non_Enzymatic_Motifs',
         'FROM': 'Leti ProQ',
         'AUTHOR': 'JOA',
         'PRODUCED': time.strftime("%d/%m/%y  %H:%M:%S"),
         'Script': __file__,
         'Source file name': f,
         'Source file size': os.path.getsize(f),
         'Source file date': time.strftime("%d/%m/%y  %H:%M:%S",
                                           time.localtime(os.path.getmtime(f))),
         'Database': 'proq sequence'}
    )

    save_as_excel(fout, readme, d)

#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
#
"""
Tools to manage .MGF files.
"""
#
import os
import re

key = 'SCANS=(\d{1,5})'


def get_mgf(txt, mgfs):
    """Yield the file paths from a list of mgf's which filenames match <txt> text.

    txt: 'INN_Vi13_T_DIG8H_10%_121003'
    mgf : '../2ms21_INN_Vi13_T_DIG8H_10%_121003.mgf'

    """
    txt = txt.split('_')
    for mgf_path in mgfs:
        mgf_basename = os.path.basename(mgf_path)
        fname, ext = os.path.splitext(mgf_basename)
        fname = fname.split('_')   # fname : [2ms21, INN, Vi13, T, DIG8H, 10% , 121003]
        #print fname
        if fname[1:] == txt:
            yield mgf_path


def filter_mgf(mgf_path, target_scans):
    """ Builds a mgf file without scans in <target_scans> list"""
    mgf_data = []
    with open(mgf_path) as f:
        txt = f.read()
        txt = txt.split('BEGIN IONS')
        for dta in txt:
            #print dta[:500]
            m = re.search(key, dta)
            if not m:
                print('not match')
                mgf_data.append(dta)
            elif int(m.group(1)) not in target_scans:
                mgf_data.append(dta)
            else:
                print('.',)   # skipped
            
    return 'BEGIN IONS'.join(mgf_data)


def extract_scans(mgf_path, target_scans, remove=False):
    """ Builds a mgf file with scans in <scan> list.

    If remove == True, prepares an mgf file with scans in list removed.

    """
    mgf_data = []
    pattern = re.compile('SCANS=(.*)')
    for part in yield_mgf_part(mgf_path):
        scans = part.split('END IONS\n')
        for scan in scans:
            try:
                string_matched = re.findall(pattern, scan)[0]
            except IndexError:
                break
            scan_number = string_matched.split('-')[0]
            if scan_number in target_scans:
                if not remove:
                    mgf_data.append(scan + 'END IONS\n')
            elif remove:
                print('here ', scan[:50])
                mgf_data.append(scan + 'END IONS\n')

    return ''.join(mgf_data)



def extract_scans_v2(mgf_path, target_scans, remove=False):
    """ Builds a mgf file with scans in <scan> list.

    If remove == True, prepares an mgf file with scans in list removed.
    v2 is for files without 'SCAN' parameter
    In this case scan is extracted from
    
    'TITLE=20140717_13_P_L_1_18_140721_02.1507.1507.5'

    """
    mgf_data = []
    pattern = re.compile('TITLE=(.*)')
    for part in yield_mgf_part(mgf_path):
        scans = part.split('END IONS\n')
        sc = 0
        ap = 0
        for scan in scans:
            sc += 1
            try:
                string_matched = re.findall(pattern, scan)[0]
                #print(string_matched)
            except IndexError:
                break
            scan_number = int(string_matched.split('.')[1])
            if scan_number in target_scans:
                if not remove:
                    mgf_data.append(scan + 'END IONS\n')
                    ap += 1
            elif remove:
                #print('scan added ', string_matched)
                mgf_data.append(scan + 'END IONS\n')
                ap += 1

    print('total scans= %i\nsaved scans= %i' % (sc, ap))
    
    return ''.join(mgf_data)



def yield_mgf_part(mgf_path, size=1350):
    """Reads mgf in chunks of size <size>"""
    fhl = open(mgf_path, 'r')
    while 1:
        txt = fhl.readlines(size)
        if not txt:
            fhl.close()
            break
        while 1:
            newline = fhl.readline()
            if newline == 'END IONS':
                txt.append(newline)
                txt.append('\n')     # add blank line
                fhl.readline()       # eliminate blank line between data
                break
            elif newline == '':
                break
            else:
                txt.append(newline)

        yield ''.join(txt)


def do_scans_list(text, linesep='\n', col_sep='\t', col=0):
    """make scan list from text"""
    scans = []
    lines = text.split(linesep)
    for line in lines:
        scan = line.split(col_sep)[col]
        scans.append(scan)
    return scans


if __name__ == "__main__":

##  ----------------  To Modify  -------------------  ##
    DIR = r"test"                                      #
    DIR_OUT = r"test"                                  #
    #                                                  #
    mgf_name = r"test_tmt_orbi_readw.mgf"              #
    archive_scans = "scans.txt"                        #
##  -----------------------------------------------   ##

    name, ext = os.path.splitext(mgf_name)
    mgf_path = os.path.join(DIR, mgf_name)
    scans_path = os.path.join(DIR, archive_scans)
    file_2_save = os.path.join(DIR_OUT, '%s_out.%s' % (name, ext))
    #
    scan_list = open(scans_path).read()
    scan_list = do_scans_list(scan_list)
    mgf = extract_scans(mgf_path, scan_list, False)
    #
    open(file_2_save, 'w').write(mgf)

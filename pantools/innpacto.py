#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

"""
tools for pandas scripts for innpacto project
"""

from matplotlib.pyplot import plot, hist, bar
from matplotlib.pyplot import figure
from matplotlib.pyplot import xscale, yscale
from matplotlib.pyplot import legend, title, xlabel, ylabel
from matplotlib.pyplot import xticks, gcf, savefig
from _tools.matplotlib_venn import venn3, venn2

from collections import defaultdict
from collections import Counter
from scipy import array
import numpy as np
import os, sys


#noinspection PyClassHasNoInit
class Paths():
    """Location of input files.
    """
    ROOT14 = os.path.expanduser('~/nbooks_data/INNPD14')
    ROOT15 = os.path.expanduser('~/nbooks_data/INNPD15')
    ROOT2015 = os.path.expanduser('~/nbooks_data/INN2015')
    ROOT_PEPT_16 = os.path.expanduser('~/nbooks_data/INNPEPT_16')
    ROOT_PROT_16 = os.path.expanduser('~/nbooks_data/INNPROT_16')
    
    #_ROOT = r'\\Server01/documentos comuns/INN/VACUNAS/WORK4JOAQUIN_ABRIL13'

    # selected files are august 2014 files from where
    # contaminated replicates were removed
    PEPTIDES = 'exp_01_11_pd_peptide_selected.xlsx'
    PROTEINS = 'exp_01_11_pd_protein_selected.xlsx'

    # filtered files are november 2014 files from where
    # contaminated replicates were removed, set at FDR 1% and
    # filtered with mass error < 5 ppm
    PEPTIDES15 = 'exp_01_11_pd_peptide_filtered.xlsx'
    PROTEINS15 = 'exp_01_11_pd_protein_filtered.xlsx'
    
    # work of 2016 with peptide shaker data and new databases
    PEPTIDES16 = 'exp_01_11_peptide.xlsx'
    PROTEINS16 = 'exp_01_11_protein.xlsx'

    PSMS = "bpsms_from_innpacto_1_11_00.xlsx"


#noinspection PyClassHasNoInit
class PsPaths():
    """Location of input files.
    """

    ROOT14 = os.path.expanduser('~/nbooks_data/INNPS14')
    ROOT15 = os.path.expanduser('~/nbooks_data/INNPS15')

    #_ROOT = r'\\Server01/documentos comuns/INN/VACUNAS/WORK4JOAQUIN_ABRIL13'
    PEPTIDES = 'exp_01_11_peptides_ps.xlsx'
    PROTEINS = 'exp_01_11_proteins_ps.xlsx'
    PSMS = "bpsms_from_innpacto_1_11_00.xlsx"


#noinspection PyClassHasNoInit
class Constants():
    COLORS = ['r', 'b', 'y', 'g', 'c', 'm', 'k']
    FIGSIZE = False
    SAVE_FIG = False


def get_definitions(keyword, *dataframes):
    """Count protein entries with a given functional classification name.

    keyword -> "Molecular Function" | "Cellular Component" |
               "Biological Process"
    dataframes -> One or more pandas DataFrames with data

    """
    keys = []
    counts = []
    for dataframe in dataframes:
        keyword_values = list(dataframe[keyword])
        definitions = [component.strip() for item in keyword_values if item != 0
                       for component in item.split(';')]
        definitions_counts = Counter(definitions)
        counts.append(definitions_counts)
        keys.extend(definitions_counts.keys())

    keys = list(set(keys))

    values = []
    for definitions_counts in counts:
        dataframe_values = array([definitions_counts.get(key, 0)
                                  for key in keys])
        values.append((100. * dataframe_values) / sum(dataframe_values))

    return keys, values


def get_key(x):
    """Get protein name from the full description line.

    Remove commas, dashes ('-') and double spaces to better compare descriptions.

    From:
        "30S ribosomal protein, S10 OS=Brachyspira hyodysenteriae (strain..."
    returns:
        "30S ribosomal protein S10"

    From:
       "Putative uncharacterized protein OS=Brachyspira hyodysenteriae \
       (strain ATCC 49526 / WA1) GN=BHWA1_01216 PE=4 ...[C0R0Q8_BRAHW]"
    returns
       "BHWA1_01216"

    """
    try:
        if 'Putative uncharacterized' in x:
            key = x.split('GN=')[1]
            key = 'PutUnchar_%s' % key.split()[0]
        else:
            key = x.split('OS')[0]
            key = key.replace('-', ' ').replace(', ', ' ').replace('  ', ' ')
    except (IndexError, AttributeError):
        key = ""
    return key


def pd_get_increase_on(items, key):
    """Get increase in number of proteins for samples defined by key.

    items: is a dictionary of data tables per replicate and per sample.
           items[klass][replicate]  -> table of identifications for a MS run
           klass -> '01_H_C_0'
           replicate -> 'B'
    key:   defines replicate and sample using a part of the sheet name to
           indicate a given group of samples.
           key = 'H_C_0' defines all replicate analyses of exopeptidome for hyodysenteriae.
    data:  'pd'/'ps' (data from protein discoverer or peptide shaker). Peptide shaker doesnt gives values for replicates

    """
    # first calculate the dictionary of average counts per key  in order to have a reference to accept the run as good
    # if the run has too few peptides/proteins in comparison with the average, the run is not included
    counter = defaultdict(list)
    for klass in items:
        if key in klass:
            for replicate in items[klass]:
                counts = len(set(items[klass][replicate]))
                counter[key].append(counts)

    averages = {}
    for key, values in counter.iteritems():
        n = len(values)
        average = 1. * sum(values) / n
        averages[key] = average

    # get the increase
    increase = []
    proteins = set()
    some_discarded = False

    for klass in items:
        if key in klass:
            for replicate in items[klass]:
                nr = set(items[klass][replicate])
                if len(nr) > 0.1 * averages[key]:
                    proteins = proteins.union(nr)
                    increase.append([klass + '_' + replicate, len(proteins)])
                else:
                    if not some_discarded:
                        print 'discarded runs for key %s (low counts)' % key
                        some_discarded = True
                    print '- %s %s %i' % (key, replicate, len(nr))
    return increase

def ps_get_increase_on(items, key):
    """Get increase in number of proteins for samples defined by key.

    items: is a dictionary of data tables per replicate and per sample.
           items[klass][replicate]  -> table of identifications for a MS run
           klass -> '01_H_C_0'
           replicate -> 'B'
    key:   defines replicate and sample using a part of the sheet name to
           indicate a given group of samples.
           key = 'H_C_0' defines all replicate analyses of exopeptidome for hyodysenteriae.
    data:  'pd'/'ps' (data from protein discoverer or peptide shaker). Peptide shaker doesnt gives values for replicates

    """
    # When Using peptide shaker data all replicates are already combined so no average can be calculated
    # get the increase
    increase = []
    proteins = set()

    for klass in items:
        if key in klass:
            nr = set(items[klass])
            proteins = proteins.union(nr)
            increase.append([klass, len(proteins)])

    return increase



def plot_increases_on(items, title_, ylabel_='NA', name='x', mode='color',
                      draw_legend=True, feedback=False, curve_fit=False, data_type='pd',  **sites):
    """Line plot data provided by get_increase_on.

    Allows plotting different klasses described in dictionary sites.
    sites: dictionary of descriptive name vs experiment key.
           experiment key: used to get all of the experiments of the same class
           name: used for labels in plotting
    mode: type of lines (color / line / both)
    feedback:
    curve_fit:

    """

    if data_type == 'pd':
        get_increase_on = pd_get_increase_on
    elif data_type == 'ps':
        get_increase_on = ps_get_increase_on
    else:
        raise ValueError("wrong data_type param, use either 'ps' or 'pd'")

    if Constants.FIGSIZE:
        figure(figsize=Constants.FIGSIZE)

    from itertools import cycle
    lines = ["-","--","-.",":"]
    if mode == 'color':
        colorcycler = cycle(Constants.COLORS)
        linecycler = cycle(['-'])
    elif mode == 'line':
        colorcycler = cycle(['k'])
        linecycler = cycle(lines)
    else:
        colorcycler = cycle(Constants.COLORS)
        linecycler = cycle(lines)


    xp = np.linspace(0, 50, 50)

    for site, key in sites.iteritems():
        increase = get_increase_on(items, key)
        values_to_plot = np.array([1. * val[1] for val in increase])
        # print '%s: %f' %(site, values_to_plot[-1])
        plot(values_to_plot, color=next(colorcycler), linestyle=next(linecycler), label=site)
        if feedback:
            z = fit_polynomial(values_to_plot)
            plot(xp, z(xp), '--')
        elif curve_fit:
            yp, popt = fit_to_func(values_to_plot)
            plot(yp, '--')

    if draw_legend:
        legend(loc='upper left')
    title(title_)
    xlabel('analysis number')
    ylabel(ylabel_)

    if Constants.SAVE_FIG:
        #savefig(name, dpi=600)
        savefig(name, dpi=1200)


def fit_polynomial(series):
    from numpy import polyfit, poly1d
    x = range(len(series))
    z = polyfit(x, series, 2)
    pz = poly1d(z)
    return pz


def fit_to_func(series):
    from scipy.optimize import curve_fit
    points = len(series)
    xp = np.linspace(0, points-1, points)

    popt = (max(series), 10, 100)
    for i in range(2):
        P, R, Z = popt
        popt, pcov = curve_fit(michaelis, xp, series, (P, R, Z))
    y = michaelis(xp, *popt)
    return y, popt


def michaelis(series, P, K, Z):
    """A Michaelis Menten with a different meaning.

               X
    y = P * --------
             X + K

    P total population
    X number of sample
    K constant

    """
    num = P * series
    div = K + series

    return (num / div) + Z


def plot_dots(df, title_='x', xlabel_='', ylabel_='', mode='linear'):
    """"""
    plot(df[xlabel_], df[ylabel_], 'o')
    xlabel(xlabel_)
    ylabel(ylabel_)
    title("%s # psm's" % title_)

    if mode == 'log':
        yscale('log')
        xscale('log')

    if Constants.SAVE_FIG:
        name = '%s_%s_%s_%s' % (title_, xlabel_, ylabel_, mode)
        savefig(name, dpi=600)


def draw_hist(data_list, param, labels=None, range_=(0, 100), bins_=50,
              title_='', xlabel_='', ylabel_='', name='out'):
    """Plot histogram

    data_list
    """
    D = len(data_list)
    if not labels or len(labels) != D:
        labels = [''] * D

    alpha = 1
    for data, color, label in zip(data_list, Constants.COLORS, labels):
        hist(data[param], color=color, bins=bins_, range=range_,
             label=label, alpha=alpha, hold=True, normed=True)
        alpha = 0.7

    legend()
    title(title_)
    ylabel(ylabel_)
    xlabel(xlabel_)

    if Constants.SAVE_FIG:
        savefig(name, dpi=600)


def draw_bar(data_list, labels=None, keys=None, title_='title', ylabel_='y label',
             separation=1, width=0.35, relative_size=1, name='out'):
    """Plot bar figure

    relative_size: to reduce the size of the figure. Keeps ratio w/h but produces problems with borders
    """
    N = len(keys)
    D = len(data_list)
    if not labels or len(labels) != D:
        labels = [''] * D

    ind = np.arange(0, separation * N, separation)
    width = 2 * width / max(2, D)
    figure(figsize=(relative_size * N, relative_size * 4))

    for idx, (data, color, label) in enumerate(
            zip(data_list, Constants.COLORS, labels)):
        bar(ind + idx * width, data, width, color=color, label=label)

    ylabel(ylabel_)
    title(title_)
    xticks(ind + width, keys, rotation=30)
    if labels:
        try:
            legend()
        except:
            pass
    #gcf().subplots_adjust(bottom=0.25)

    if Constants.SAVE_FIG:
        #savefig(name, dpi=600)
        savefig(name, dpi=1200)  # frontiers hr


def plot_venn(groups, labels, title_, name, transform='', set_numbers=True):
    """Plot venn diagrams with 2 or 3 circles.

    The program needs to calculate the different regions from the original
    collections in <groups>.
    For two groups:
    (Ab, aB, AB)        # lowercase group means 'and not group'
    For three groups:
    (Abc, aBc, ABc, abC, AbC, aBC, ABC)

    """
    alpha = 0.5
    data = []
    if len(groups) == 3:
        a, b, c = groups
        data = map(len, [a.difference(b, c),
                         b.difference(a, c),
                         a.intersection(b).difference(c),
                         c.difference(a, b),
                         a.intersection(c).difference(b),
                         b.intersection(c).difference(a),
                         a.intersection(b, c)])

        if transform:
            data_t = np.sqrt(data)
        else:
            data_t = data
        venn3(subsets=data_t, alpha=alpha, set_labels=labels, set_numbers=set_numbers)
        title(title_, fontsize=16)
    elif len(groups) == 2:
        a, b = groups
        data = map(len, [a.difference(b),
                         b.difference(a),
                         a.intersection(b)])

        if transform:
            data_t = np.sqrt(data)
        else:
            data_t = data
        
        venn2(subsets=data_t, alpha=alpha, set_labels=labels, set_numbers=set_numbers)
        title(title_, fontsize=16)

    if Constants.SAVE_FIG:
        savefig(name, dpi=600)

    return map(len, groups), data


def get_members_of(subjects, descriptions, replicates):
    """Return list of component descriptions for each type of sample.

    Components can be p.e. peptides or proteins.
    subjects: dictionary giving the code for each type of sample.
        keys: type of sample, like 'exopeptidome'.
        values: code tag that identifies the type of sample, like 'C_0'.

    descriptions: dictionary of dictionaries of descriptions or sequences.
        keys: codes that identify the sample, like '01_H_C_0'.
        values: dictionary of descriptions for each replicate on the sample.
            key: replicate code, like 'A'.
            values: list of identifiers for the components in the replicates.
                for proteins: the uniprot description.
                for peptides: the sequence.

    """
    members = defaultdict(list)
    for matrix, code_tag in subjects.iteritems():
        for code in descriptions:
            if code_tag in code:
                for replicate in replicates:
                    # print name, replicate
                    members[matrix].extend(descriptions[code][replicate])
    return members

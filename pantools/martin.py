#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

"""
tools for analyzing MS/MS identifications related to proq
"""

from matplotlib.pyplot import plot
from matplotlib.pyplot import figure
from matplotlib.pyplot import xlim, ylim
from matplotlib.pyplot import legend, title, ylabel
from matplotlib.pyplot import savefig, gca
from matplotlib.ticker import MultipleLocator
from itertools import cycle
from innpacto import Constants
from undercover.proteins import protein_1 as proq

import re


def get_no_tryptic(x):
    """Search non-enzymatically generated peptides in a digest of a protein

    To be used in pandas with map.

    """
    index = proq.find(x)
    if index == -1:
        return 0, 0, 0
    if x.endswith('R') or x.endswith('K'):
        previous = proq[index - 1]
        if previous not in 'RK':
            return 'N-term', previous, index
        else:
            return 0, 0, 0
    else:
        last_aa_index = index + len(x) - 1
        return 'C-term', proq[last_aa_index], last_aa_index + 1


def get_no_gluc(x):
    """Search non-enzymatically generated peptides in a digest of a protein.

    To be used in pandas with map

    """
    index = proq.find(x)
    if index == -1:
        return 0, 0, 0
    if x.endswith('E') or x.endswith('D'):
        previous = proq[index - 1]
        if previous not in 'ED':
            return 'N-term', previous, index
        else:
            return 0, 0, 0
    else:
        last_aa_index = index + len(x) - 1
        return 'C-term', proq[last_aa_index], last_aa_index + 1


def remove_ptms(x):
    """Remove ptms from x.

    To be used in pandas with map
    """
    new = re.sub('\(\d*\)', '', x)
    return new


def plot_no_enzymatic(series, sheets, title_='', size=(15, 9),
                      sep=3, high = 7, mj_loc=20, save=False):
    """Make figure with multiple, piled up, plots from a dict of pandas series.

    series: dict of panda series.
    sheets: keys from panda series <series> dict to plot.
    size:  size of the figure in inches.
    sep:   distance between plots in y-units.
    high:  length of the y-axis range.
    mj_loc: major locator ticks label distance.

    """
    majorLocator = MultipleLocator(mj_loc)
    minorLocator = MultipleLocator(5)

    color = cycle(Constants.COLORS)

    n = 0
    number = len(sheets)
    high = high - sep
    figure(figsize=size)
    for sheet in sheets:
        positions = series[sheet]
        #                     max  y-pos      last at 1
        positions = [value + (sep * number) - (sep - 1) - (sep * n)
                     for value in positions]
        plot(range(1, len(positions) + 1), positions, next(color), label=sheet)
        n += 1

    title(title_)
    ax = gca()
    ax.xaxis.set_major_locator(majorLocator)
    ax.xaxis.set_minor_locator(minorLocator)
    legend()

    xlim(0, 470)
    ylim(0.5, (sep * number) + high)
    ylabel('# of peptide psm')

    if save:
        savefig(save, dpi=600)
"""
Reads a list of sequences (a column) from an archive <sequence_file>.
Calculates number of times a series of amino acids <searched_aas> are repeated
 in each sequence.
Generates a file with a column of sequences and one or more columns of
 repetitions
"""
import os


def read_sequence_file(filepath):
    sequences = []

    for line in open(filepath, 'r'):
        sequences.append(line.strip())
    return sequences

def count_aas(sequences, searched_aas):
    """"""
    lines = []
    for sequence in sequences:
        line_items = [sequence]
        for amino_acid in searched_aas:
            counts = sequence.count(amino_acid)
            counts = str(counts)
            line_items.append(counts)

        line = "\t".join(line_items)

        lines.append(line)
    return lines

def save(lines, path):
    """"""
    text = "\n".join(lines)
    open(path, 'w').write(text)


if __name__ == '__main__':
    #
    TEST = True
    #
    ####################  To Modify ##########################
    #                                                        #
    DIR = "test"                                             #
                                                             #
    file_sequences = ".txt"                                   #
    file_to_save = r".xls"                                   #
    #                                                        #
    searched_aas = ["s", "t", "y"]                           #
    #                                                        #
    ##########################################################
    #
    if TEST:
        DIR = 'test'
        file_sequences = "p_peptides_ascore_15_for_test.txt"
        file_to_save = "count_psites_test_out.xls"

    file_sequences = os.path.join(DIR, file_sequences)
    file_to_save = os.path.join(DIR, file_to_save)

    sequences = read_sequence_file(file_sequences)
    aa_counts = count_aas(sequences, searched_aas)
    save(aa_counts, file_to_save)

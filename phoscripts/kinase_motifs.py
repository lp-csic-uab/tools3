"""
Determines the motif type of a phosphorylated sequence.
Uses two motifs lists from two different articles.
One is given as a file and the other is a list <motifs_list>.
"""
#
import re
import os
from numpy import array
from collections import defaultdict
#
motifs_list = [("CDK125",   "[st]P.[RHK]"),  ("ERK12",   "P.[st]P"),
               ("Pro_dir",  "[st]P"),        ("GSK3 ",   "[st]...{ST}"),
               ("CK1",      "D..[st]"),      ("CK2  ",   "[st]..[DE]"),
               ("PDK1",     "[st]YF"),       ("PDK2",    "[YF][st][YF]"),
               ("baso_1",   "[RK].[st]"),    ("baso_2",  "[RK]..[st]"),
               ("14-3-3_A", "R..s"),         ("14-3-3_B", "R...s"),
               ("Akt",      "RSK[RK].[RK]..[st]")
               ]

#
def read_sequence_file(filepath):
    """Read a file of sequences into a list"""
    sequences = []

    for line in open(filepath, 'r'):
        sequences.append(line.strip())
    return sequences
#
def read_motif_file(filepath):
    """Read a file of motif patterns into a list"""
    motifs = []
    for line in open(filepath, 'r'):
        par = line.strip().split()
        if len(par) > 1:
            motifs.append(par)
    return motifs
#
def enlarge_sequences(sequences, extend=10, dbase='', all_seqs=True):
    """Extend by <extend> amino acids the C- and N-terms of a sequence

    Search the sequence in a dbase <dbase>.
    Returns a dictionary  dict(original=extended,).

    First creates a combined sequence string made of all database sequences:
    "**********-PROTEINA-PROTEINB-......-PROTEINN-**********"
    And then it searches for the matches in this string.

    - if all_seqs is False:
        search for the first match and it doesnt takes into account
        possible homologous proteins having the same peptide nor repeated
        peptides in the same protein.
        This is a plainly wrong search but faster than searching with
         all_seqs=True.
    - if all_seqs is True:
        search for all matches in all database proteins

    PROBLEM
    - This full string strategy can be a memory problem with big databases.

    """
    from Bio import SeqIO

    # long * string to prevent index out of range ?
    extreme = '*' * extend
    dbase_secs = [extreme]

    with open(dbase, 'r') as handle:
        for seq_record in SeqIO.parse(handle, "fasta"):
            dbase_secs.append(str(seq_record.seq))

    dbase_secs.append(extreme)
    dbase_txt = '-'.join(dbase_secs)

    enlarged = defaultdict(set)

    for sequence in sequences:
        up_sequence = sequence.upper()
        index = dbase_txt.find(up_sequence)
        if index >= 0:
            length = len(up_sequence)
            while index >= 0:
                end_ext_sec = index + length + extend
                tail = dbase_txt[index - extend:index]
                head = dbase_txt[index + length:end_ext_sec]
                sec = tail + sequence + head
                if '-' in sec:
                    pieces = sec.split('-')
                    sec = max(pieces, key=len)
                enlarged[sequence].add(sec)
                if all_seqs is False:
                    break

                index = dbase_txt.find(up_sequence, end_ext_sec)
        else:
            print 'not found %s' % sequence
            enlarged[sequence].add(sequence)

    return enlarged
#
def enlarge_sequences_all(sequences, extend=10, dbase='', all_seqs=False):
    """Deprecated function.
     Use enlarge_sequences with parameter all_seqs=True
     """
    raise Exception("Removed function."
                    "Use enlarge_sequences with parameter all_seqs=True")

def get_phospho_targets_all(extended, extend=5):
    """Deprecated function.
     Used to distinguish from get_phospho_targets when this function
     expected a <extended> dict with only a value (extended sequence) per key
     (original peptide).
     """
    raise Exception("Removed function. Use get_phospho_targets")
#
def get_phospho_targets(extended, extend=5):
    """From a dictionary of extended sequences get p-site-centered targets

    input -> {'ADFsGRDFtADF': set(['XXXXADFsGRDFtADFYYYY', ...]), ...}
    output -> {'ADFsGRDFtADF': ['XXADFsGRDFT', 'SGRDFtADFYY', ...], ...}

    """
    new = {}
    for key, seqs in extended.iteritems():
        targets = set()
        for seq in seqs:
            upper = seq.upper()
            for idx, aa in enumerate(seq):
                if aa.islower():
                    add_tail = add_head = 0
                    start = idx - extend
                    if start < 0:
                        add_tail = - start
                        start = 0

                    end = idx + extend
                    if end >= len(upper):
                        add_head = end - len(upper) + 1
                        end = len(upper)

                    target = upper[start:idx] + aa + upper[idx + 1:end + 1]
                    target = '-' * add_tail + target + '-' * add_head
                    targets.add(target)
        new[key] = list(targets)

    return new
#
def build_empty_sequence_dict(sequences):
    """Builds an empty dictionary to hold sequence data.

    dictionary type: dictionary['sequence'] = []
    TODO: could be replaced by defaultdict(list)

    """
    dictionary = {}
    for sequence in sequences:
        if sequence:
            dictionary[sequence] = []
    return dictionary
#
def get_matching_motifs(motifs, sequences):
    """Get motifs for each sequence in sequences list.

    is insert(pos,item) necessary? --> simply append(item) ???

    """
    for sequence in sequences:
        new_list = []
        for motif in motifs:
            #print patron
            if re.findall(motif[1], sequence):
                new_list.append(motif[0])
        print new_list
        sequences[sequence].append(new_list)
#
def adjust(motifs_dict):
    """

    motifs_dict : dictionary of type:
        motifs_dict['sequence'] = [list_1,...,list_n]
        where list_i - list_n are lists of motifs from different collections

    """
    tabla = []
    for sequence, motif_lists in motifs_dict.iteritems():
        row = [len(motif_list) for motif_list in motif_lists]
        tabla.append(row)
    table_array = array(tabla)
    maxs = table_array.max(0)
    print maxs

    for sequence, motif_lists in motifs_dict.iteritems():
        idx = 0
        for motif_list in motif_lists:
            start = len(motif_list)
            stop = maxs[idx]
            if stop > start:
                for pos in range(start, stop):
                    motif_list.insert(pos, '*')

            idx += 1
#
def get_kinases(sequence_file, motif_file, base_path='out'):
    """Get kinases for the phospho-motifs in a list of sequences.

    """
    # read file with phospho sequences
    sequences = read_sequence_file(sequence_file)
    # build a dictionary with sequences enlarged at their extremes
    enlarged = enlarge_sequences(sequences, extend=5, dbase=dbase)
    # build a list of motifs (regexes)
    motifs = read_motif_file(motif_file)
    # search a motifs for each sequence in the dictionary
    full_data = build_empty_sequence_dict(enlarged.values())
    get_matching_motifs(motifs, full_data)
    # same for the other group of motifs
    get_matching_motifs(motifs_list, full_data)
    # build a list of motifs frequencies
    frequencies_1 = defaultdict(int)
    frequencies_2 = defaultdict(int)

    for (list_1, list_2) in full_data.values():
        for motif in list_1:
            frequencies_1[motif] += 1
        for motif in list_2:
            frequencies_2[motif] += 1

    # fill empty places with '*' pto build a data table
    adjust(full_data)
    lines = []

    for sequence in sequences:
        print sequence
        enlarged_sequence = enlarged[sequence]
        sequence_motifs_lists = full_data[enlarged_sequence]

        partial = [sequence, enlarged_sequence]
        for sequence_motifs in sequence_motifs_lists:
            txt = "\t".join(sequence_motifs)
            partial.append(txt)
        txt = "\t".join(partial)
        lines.append(txt)
        #
    text_2_save = "\n".join(lines)

    file_to_save = '%s_%s.xls' % (base_path, 'out')
    open(file_to_save, 'w').write(text_2_save)
    #
    # now save the file with the frequencies for each motif
    text = ''
    list_2 = [item for item, motif in motifs_list]
    list_1 = [item[0] for item in motifs]

    for dictionary, keys in [(frequencies_1, list_1), (frequencies_2, list_2)]:
        for key in keys:
            num = dictionary[key]
            text += key + '\t' + str(num) + '\n'

    file_to_save = '%s_%s.xls' % (base_path, 'freqs')
    open(file_to_save, 'w').write(text)
#
def get_phosphosites(sequences, dbase, outfile=None, sides=5):
    """
    Build a p-sites dictionary from sequences enlarged in both extremes
    """
    # build a dictionary with sequences enlarged in both extremes
    enlarged = enlarge_sequences(sequences, extend=sides, dbase=dbase)
    phospho_targets = get_phospho_targets(enlarged, extend=sides)
    target_dict = defaultdict(list)
    for sequence, targets in phospho_targets.iteritems():
        for target in targets:
            target_dict[target].append(sequence)

    if outfile:
        data = []
        for target, sequences in target_dict.iteritems():
            line = '%s\t%s' % (target, '\t'.join(sequences))
            data.append(line)
        text = '\n'.join(data)
        open(outfile, 'w').write(text)

    return target_dict
#
def get_phosphosites_from_file(peptide_file, dbase, *args, **kwds):
    """ Reads a file with phosphorylated peptide sequences"""
    sequences = read_sequence_file(peptide_file)
    get_phosphosites(sequences, dbase, *args, **kwds)
#
#
if __name__ == '__main__':
    #
    TEST = True
    #
    ####################  To Modify ##########################
    #                                                        #
    DIR = "test"
    DIR_DBASE = "C:/Blast/DBs"
    #
    file_sequences = "Fosfopeptidos_AscoreMayorde15.txt"     #
    #file_sequences = "test_QKsDAEE.txt"                     #
    #file_sequences = "Fosforilados_TOTAL.txt"               #
    #                                                        #
    file_motifs = r"CuratedPhosphoMotifs.txt"                #
    #                                                        #
    file_to_save_1 = "grabado2013"                           #
    file_to_save_2 = "fosfotargetsb"                     #
    #                                                        #
    dbase = "Swiss_human_reduced.fasta"                      #
    #                                                        #
    ##########################################################
    #
    if TEST:
        DIR = DIR_DBASE = 'test'
        file_sequences = "p_peptides_ascore_15_for_test.txt"
        file_motifs = r"curated_p_motifs.txt"
        dbase = "swiss_human_reduced.fasta"
        file_to_save_1 = "test_1"
        file_to_save_2 = "test_2"

    file_sequences = os.path.join(DIR, file_sequences)
    file_motifs = os.path.join(DIR, file_motifs)
    file_to_save_1 = os.path.join(DIR, file_to_save_1)
    file_to_save_2 = os.path.join(DIR, file_to_save_2)

    dbase = os.path.join(DIR_DBASE, dbase)
    #
    get_kinases(file_sequences, file_motifs, file_to_save_1)
    #
    #get_phosphosites_from_file(file_sequences, dbase,
    #                           outfile=file_to_save_2,
    #                           sides=5)

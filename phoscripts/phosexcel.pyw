#!/usr/bin/env python
# -*- coding: latin-1 -*-
"""
version 0.1.6 10 Julio 2007
Determines MS2/MS3 pairs from Dale lists.
"""
TITLE = ' Kim Phospho vs.0.1.6'
#
import os
import wx
import win32com.client
import pywintypes
from search_phos import find_peptides
#
text = (
    "\t     KIM PHOSPHO SEARCH\n\t\tvs 0.1.6\nDetermines MS2/MS3 pairs "
    "reflecting the presence of a phospho-peptide from a Dale excel file "
    "where spectra that produced MS3 (phosphate is detected) have been "
    "shorter by sequence"
)
#
###        Defaults        ###
##                           #
default_xls = "C:/nofile"    #
default_sheet = 'Sheet1'     #
default_col_sequence = 1     #
default_col_mark = 2         #
default_row_ini = 1          #
##                           #
##                         ###
#

class PhosphoDialog(wx.Frame):
    """GUI for data entry"""

    windows = dict(
        col_sequence=('data column', ''),
        col_mark=('target column', ''),
        row_ini=('init row', '')
    )
    checkboxes = dict(
        insert=('insert', "Insert a new column st given position"),
        color=('color', "Color in yellow rows with phospho-peptides"),
        show=('open', "Open and modifies on-line a document")
    )

    def __init__(self, parent, xls=default_xls, sheet=default_sheet,
                 col_sequence=default_col_sequence, col_mark=default_col_mark,
                 row_ini=default_row_ini):
        wx.Frame.__init__(self, parent, -1, TITLE, size=(300, 340))
        #
        self.xls = xls
        self.xls_dir, self.xls_name = os.path.split(self.xls)
        self.sheet = sheet
        self.col_sequence = col_sequence
        self.col_mark = col_mark
        self.row_ini = row_ini
        self.choices = [self.sheet]
        self.color = None
        self.insert = None
        self.show = None
        #
        self.panel = wx.Panel(self)
        #
        wx.TextCtrl(self.panel, value=text,
                    style=wx.TE_MULTILINE | wx.TE_READONLY,
                    pos=(10, 10), size=(275, 45)
                    )
        self.txt_file = wx.TextCtrl(self.panel, value=self.xls_name,
                                    style=wx.TE_READONLY,
                                    pos=(50, 80), size=(150, 22)
                                    )
        self.but_file = wx.Button(self.panel, label='...',
                                  pos=(200, 80), size=(20, 22)
                                  )
        self.list_sheet = wx.Choice(self.panel, choices=self.choices,
                                    pos=(50, 105), size=(100, 22)
                                    )
        wx.StaticText(self.panel, label='Sheet',
                      pos=(160, 110), size=(80, 22))
        #
        self.list_sheet.Select(0)
        #
        ypos = 140
        self.properties = {}
        for name, labels in PhosphoDialog.windows.iteritems():
            valor = str(getattr(self, name))
            win = wx.TextCtrl(self.panel, value=valor,
                              pos=(50, ypos), size=(60, 20))
            self.properties[name] = win
            wx.StaticText(self.panel, label=labels[0],
                          pos=(120, ypos + 2), size=(80, 22))
            ypos += 30
            #
        xpos = 50
        for name, labels in PhosphoDialog.checkboxes.iteritems():
            win = wx.CheckBox(self.panel, label=labels[0],
                              pos=(xpos, 230), size=(60, 22))
            self.properties[name] = win
            self.properties[name].SetToolTipString(labels[1])
            xpos += 65
            #
        self.but_run = wx.Button(self.panel, label='POS DALE', pos=(110, 265))
        #
        self.Bind(wx.EVT_CLOSE, self.on_close_window)
        self.Bind(wx.EVT_BUTTON, self.on_run, self.but_run)
        self.Bind(wx.EVT_BUTTON, self.on_search_file, self.but_file)

    def on_search_file(self, evt):
        """Opens a excel file and display the sheet names in a ChoiceList.
        """
        dlg = wx.FileDialog(self, "Select File", defaultDir=self.xls_dir,
                            style=wx.OPEN)
        if dlg.ShowModal() == wx.ID_OK:
            self.xls_dir = dlg.GetDirectory()
            self.xls_name = dlg.GetFilename()

            self.txt_file.SetValue(self.xls_name)
            self.xls = os.path.join(self.xls_dir, self.xls_name)

            xl = win32com.client.Dispatch("Excel.Application")
            wb = xl.Workbooks.Open(self.xls)
            sh_number = wb.Sheets.Count
            self.choices = []
            for i in range(1, sh_number + 1):
                name = wb.Sheets.Item(i).Name
                self.choices.append(name)
            wb.Close()
            xl.Quit()
            self.list_sheet.SetItems(self.choices)
            self.list_sheet.Select(0)

    def on_run(self, evt):
        try:
            if self.xls_name == 'nofile':
                return
            if not os.path.exists(self.xls):
                return
            self.run()
        except pywintypes.com_error:
            #self.xl.Quit()
            print 'error'

    def run(self):
        """"""
        for name in PhosphoDialog.checkboxes:
            val = self.properties[name].GetValue()
            setattr(self, name, val)
            #
        self.sheet = self.choices[self.list_sheet.GetSelection()]
        #
        self.xl = win32com.client.Dispatch("Excel.Application")
        if self.show:
            self.xl.Visible = 1
        self.wb = self.xl.Workbooks.Open(self.xls)
        ws = self.wb.Sheets(self.sheet)
        ws.Select()
        #
        for name in PhosphoDialog.windows:
            val = int(self.properties[name].GetValue())
            setattr(self, name, val)
            #
        pept_list = []

        for i in range(self.row_ini, 1000):
            value = ws.Cells(i, self.col_sequence).Value
            if value:
                pept_list.append(value)
            else:
                break
            #
        mark = find_peptides(pept_list)
        #
        if self.insert:
            target = ws.Columns(self.col_mark)
            target.Select()
            target.Insert(1)
            #
        for i in range(self.row_ini, 1000):
            if (i - 1) in mark:
                v = i + self.row_ini
                ws.Cells(v - 1, self.col_mark).Value = '*'
                ws.Cells(v, self.col_mark).Value = '*'

                if self.color:
                    x1 = str(v - 1) + ":" + str(v)
                    range_ = ws.Rows(x1)
                    range_.Select()
                    interior = range_.Interior
                    interior.ColorIndex = 6

        self.wb.Close()
        self.xl.Quit()

    def on_close_window(self, evt):
        self.Destroy()


if __name__ == "__main__":
    app = wx.PySimpleApp()
    PhosphoDialog(None).Show()
    app.MainLoop()

#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
script_check_mod_xml

- check_xml
Detects malformed XML files in a directory.
Once corrected psearch can be applied without annoying errors

- mod_xml
fix lack of '>' tag in BioWorks files
September 1, 2009

"""
#
#
import os
import wx
from cStringIO import StringIO
import traceback
import xml.etree.ElementTree as et
from xml.parsers.expat import ExpatError
from commons.warns import tell

what = 1


def check_xml(xml):
    """"""
    archive = "C:/errors.txt"
    error = []
    global what
    #
    try:
        et.parse(xml)
    except ExpatError, e:
        text = "...%s produce error\n'%s'" % (xml[-20:], e)
        text_2 = "\nOK to continue with warnings\nCANCEL to skip warnings"
        if what:
            what = tell(text + text_2, level='fatal')
    else:
        text = '...%s  OK' % xml[-20:]
    #
    error.append(text)
    open(archive, 'w').write("\n".join(error))
    os.startfile(archive)


def get_next_xml(xml):
    """"""
    #
    at_least_one = False
    #
    if os.path.isfile(xml):
        yield xml
    elif os.path.isdir(xml):
        for fxml in os.listdir(xml):
            fxml_path = os.path.join(xml, fxml)
            if os.path.isfile(fxml_path) and fxml_path.endswith('.xml'):
                at_least_one = True
                yield fxml_path
        if not at_least_one:
            tell('There are no workable files in %s' % xml)
            yield None


def mod_xml(filepath):
    """ """
    old = """Schema-instance" <origfilename>"""
    new = """Schema-instance"><origfilename>"""
    txt = open(filepath).read()
    txt = txt.replace(old, new, 1)
    open(filepath, 'w').write(txt)


def main():
    """Select a function and the files to apply."""
    funcs = (mod_xml, check_xml)
    #noinspection PyUnusedLocal
    App = wx.PySimpleApp()
    dialog = wx.SingleChoiceDialog(None, "select one",
                                   "Available methods",
                                   ['modify xml', 'check xml'])
    message = dialog.ShowModal()
    if message == 5101:
        return
    choice = dialog.GetSelection()
    print choice
    func = funcs[choice]
    dir_path = wx.DirSelector()
    if dir_path:
        # noinspection PyBroadException
        try:
            for filepath in get_next_xml(dir_path):
                print dir_path
                func(filepath)
        except Exception:
            f = StringIO()
            traceback.print_exc(file=f)
            valor = f.getvalue()
            tell('Error NO completed \n\n%s' % valor)


if __name__ == "__main__":

    # functions to choose -> mod_xml, check_xml

    main()

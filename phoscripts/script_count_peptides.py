#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
script_count_peptides_02
script for plasma peptide data
october 2009
"""
#
import os
import numpy
from matplotlib import interactive
interactive(True)
#from matplotlib import pyplot as pp
from collections import defaultdict
from sequence_converters import from_brackets_to_ampersat
#
#
def load_peptides(path, col=7, nrd=False, keep_row=True):
    """Reads column of peptide sequences from tsv file"""

    peptides = []

    for line in open(path):
        line_items = line.split("\t")
        peptide = line_items[col].strip()
        #
        if (not peptide) or (nrd and peptide in peptides):
            if keep_row:
                peptides.append(":")
            continue
        #
        peptides.append(peptide)

    return peptides
#
#
def analyze_peptides(peptides, nrd=False):
    """Counts psites in a list of sequences

    returns list of lists of peptide parameters:
            (#aas, #phosphates, #missed cleavages, #basic aas)

    """
    all_counts = []
    already_seen = []

    for peptide in peptides:
        if peptide == ':':
            all_counts.append((-1, -1))
            continue
        #
        if nrd:
            if peptide in already_seen:
                all_counts.append((-1, -1))
                continue
            already_seen.append(peptide)
        #
        counts = count_it(peptide)
        all_counts.append(counts)

    return all_counts
#
#
def format_sequences(peptides, convert):
    return from_brackets_to_ampersat(peptides, inverse=convert)
#
#
def save_list(peptides_counts, path):
    """Save list of lists as tsv file"""
    lines = []
    #
    for counts in peptides_counts:
        line_items = [str(n) for n in counts]
        line = '\t'.join(line_items)
        lines.append(line)

    text = '\n'.join(lines)
    open(path, 'w').write(text)
#
#
def dic_from_list(peptides_counts):
    """Build a dictionary with p-peptide statistics data

    Used for calculations
    peptides_counts: [(phos, aas, miss_clv, basic_aas), ..., ()]
    returns: dictionary[phos] = [(aas, miss_clv, basic_aas), ..., ()]

    """
    dic = defaultdict(list)
    for counts in peptides_counts:
        dic[str(counts[0])].append(counts[1:])

    return dic
#
#
def count_it(peptide):
    """Counts total amino acids, phosphates and missed cleavages in peptide.

    Sequences are of the type: '-.ASDF$M@ASFR.T'

    """
    #
    try:
        peptide = peptide.split('.')[1]
    except IndexError:
        pass
    except AttributeError:
        return -1, -1
    #
    count_aas = 0   # aas
    count_p = 0     # phosphates
    count_m = 0     # missed cleavages
    count_b = 0     # basic aas (H,P)
    #
    for item in peptide:
        if item.isalpha():
            count_aas += 1
        if item in ['#', '@', 's', 't', 'u', 'v', 'y']:
            count_p += 1
            continue
        if item in ['K', 'R']:
            count_m += 1
        if item in ['P', 'H']:
            count_b += 1
    # K, R terminal do not count as missed
    if peptide[-1] in ['K', 'R']:
        count_m -= 1
    # K y R do not cut at Proline
    if "KP" in peptide:
        count_m -= 1
    if "RP" in peptide:
        count_m -= 1
    if count_m > 2:
        print peptide
    #
    return count_p, count_m, count_aas, count_b
#
#
#
if __name__ == '__main__':

    TEST = True
    #
    ####   ******************* start user data  ******************   ##
    ##     *****   fi = (archivo, num_column, nr, convert)   *****   ##
    #
    DIR = os.path.expanduser("~/Desktop/datos_totales_fosfoplasma/Resultados")
    f1 = ("fp_pool2_lap_1fdr.xls", 7, True, True)
    f2 = ("fp_pool2_lap_1fdr_no23.xls", 7, True, True)
    f3 = ("Ziel.xls", 0, True, True)
    f4 = ("total_p-np_fdr_1.xls", 0, True, True)
    f5 = ("tests.xls", 0, True, True)
    f6 = ('total_mas1_1fdr.xls', 0, True, True)
    f7 = ('Villen_psequences2.xls', 0, True, True)
    f8 = ("total_fdr1.xls", 0, True, True)
    #
    files2run = (f3, f7, f8)
    ####   ******************* end user data  ******************   ##
    #
    if TEST:
        DIR = "test"
        f_test = ("test_count_lap_1fdr.xls", 7, True, True)
        files2run = (f_test,)
    save_as = '.xls'
    #
    for filename, column, redundancy, convert in files2run:
        filepath = os.path.join(DIR, filename)
        to_save = os.path.splitext(filepath)
        save_path = to_save[0] + "_out" + save_as
        #
        peptides = load_peptides(filepath, column, redundancy)
        #
        peptides = format_sequences(peptides, convert)
        peptides_counts = analyze_peptides(peptides, redundancy)
        print peptides_counts
        #
        counts = dic_from_list(peptides_counts)
        print counts
        #
        #pp.plot(xx, yy, 'b.')
        save_list(peptides_counts, save_path)
        #
        print filename
        print 'p    mc   aas  basic'
        more_than_2 = 0
        for key, values in counts.iteritems():
            if key == '-1':
                continue
            try:
                #print value
                for item in values:
                    if item[0] > 2:
                        more_than_2 += 1
            except IndexError:
                print values
            values = numpy.array(values)
            avg = tuple(numpy.average(values, 0))
            #print avg
            print key, '  %.1f' * len(avg) % avg

        print '%s with more than 2 missed cleavages' % more_than_2
        print 'total ', len(peptides_counts)
        print '**'

    raw_input('press any key to exit')

##    - peptide
##    - EPCVESLVSQYFQTVTDYGK
##    - AFLEVNEEGSEAAASTAVVIAGR
##    .........................
##    - DLATVYVDVLK
##    - NPSSAGSWNSGSSGPGSTGNR
##    p    mc   aas  basic
##    1   1.1  18.1  0.8
##    0   0.2  18.4  0.9
##    3   2.0  27.0  0.0
##    2   1.1  19.2  0.6
##    6 with more than 2 missed cleavages
##    total  764
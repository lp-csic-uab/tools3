#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
script_count_p_sites_04
script for plasma peptide analysis
#
do not distinguish sADtSTR <--> SADtSTR (same psite but different motif)
in both cases uses same key -> SADtSTR
october 2009
"""
#
import os
from collections import defaultdict
#
#
def load_p_sites(path, col=7, nrd=False, keep_row=True):
    """Reads peptide column from tsv file"""

    peptides = []

    for line in open(path):
        line_items = line.split("\t")
        peptide = line_items[col].strip()
        #
        if (not peptide) or (nrd and peptide in peptides):
            if keep_row:
                peptides.append(":")
            continue
        #
        peptides.append(peptide)

    return peptides
#
#
def do_dict_psites(sequences):
    """Generates psite dictionary.

    keys are like ABCsCDE where all aas but the central phosphorylated are
    uppercase:
    sADtSTR, SADtStR -- common key --> SADtSTR.

    """
    psites = defaultdict(list)

    for sequence in sequences:
        for position, amino_acid in enumerate(sequence):
            if amino_acid in ['s', 't', 'y']:
                series = ['*', '*', '*', '*', '*', '*', '*']
                for idx in range(0, 7):
                    aa_idx = position - 3 + idx
                    if aa_idx < 0:
                        continue
                    try:
                        letter = sequence[aa_idx]
                        series[idx] = letter.upper() if idx != 3 else letter
                    except IndexError:
                        continue
                #
                data = new_key(series, psites)
                if data:
                    psites[data].append(sequence)
                else:
                    psites[tuple(series)].append(sequence)

    return psites
#
#
def new_key(key, dictionary):
    """Inform about existence of similar keys in dictionary.

    keys **CsDEF y ABCsDEF are considered equals

    """
    for item in dictionary:
        value = True
        for new, stored in zip(key, dictionary):
            if new == "*" or stored == '*':
                continue
            if new != stored:
                value = False
                break
        if value:
            return item

    return None
#
#
def save_dict(dictionary, path, extra_data=None):
    """save dictionary as a text file"""

    data = []

    for item, values in dictionary.iteritems():
        text = ''.join(item) + '\t' + '\t'.join(values)
        data.append(text)

    fulltext = '\n'.join(data)

    add = "\n\nEXTRA DATA\n\n%s" % extra_data if extra_data else ""

    open(path, 'w').write(fulltext + add)
#
#
def statistics(dictio):
    """calculates number of Ser, Thr and Tyr psites"""

    p_aas = [item[3] for item in dictio]
    p_ser = p_aas.count('s')
    p_thr = p_aas.count('t')
    p_tyr = p_aas.count('y')

    return len(p_aas), p_ser, p_thr, p_tyr
#
#
#
if __name__ == '__main__':

    TEST = True
    #
    ###  ************** start user data  *************  ###
    ##    ******   fi = (file, column number)   ******   ##
    #                                                     #
    DIR = "C:/montse"                                     #
    f1 = ("fdr1_2engines_highascore.txt", 0)              #
    #                                                     #
    files2run = (f1, )                                    #
    ###  ************** end user data  **************   ###
    #
    if TEST:
        DIR = "test"
        file_test = ("test_psites.txt", 0)
        files2run = (file_test,)

    save_as = '.xls'
    #
    for filename, column in files2run:
        filepath = os.path.join(DIR, filename)
        to_save = os.path.splitext(filepath)
        save_path = to_save[0] + "ps_out" + save_as

        psites = load_p_sites(filepath, column, True)
        psite_dict = do_dict_psites(psites)
        stat_data = statistics(psite_dict)

        form = "total p-sites = %i\npser = %i\npthr= %i\nptyr= %i"
        extra_text = form % stat_data

        save_dict(psite_dict, save_path, extra_data=extra_text)

##    for item, value in psite_dict.iteritems():
##        if len(value) > 1 : print '-', item, value

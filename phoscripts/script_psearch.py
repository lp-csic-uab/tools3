#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
script_psearch_04
Manual module
october 2009
"""
try:
    import psyco
except ImportError:
    from _psearch.psrch_constants import MockPsyco
    psyco = MockPsyco()

import os
import wx
from _psearch.psrch_funcs import get_next_xml_raw, excel_from_xml
from _psearch.psrch_funcs import save_list_of_lists, save_dict_of_lists
from _psearch.psrch_quant import add_quant_from_raw
from _psearch.psrch_funcs import get_data_ms2_ms3
from _psearch.psrch_dale_funcs import read_dict_from_list, color
from _psearch.psrch_dale_funcs import find_best_line
from _psearch.psrch_dale_funcs import add_ms2_ms3_from_list
from _psearch.psrch_dale_funcs import filter_identifications

try:
    #noinspection PyUnresolvedReferences
    from matplotlib import interactive
except ImportError:
    print "Warning, matplotlib not found"
    def plot(*args):
        pass
    def figtext(*args):
        pass
    def title(*args):
        pass
    def hold(*args):
        pass
else:
    interactive(True)
    from pylab import plot, figtext, title, hold
#
#
def draw(groups, title_, texts):
    """"""
    hold(True)
    title(title_[:-2])
    pos = 0.85
    col = color()
    for txt in texts:
        figtext(0.28, pos, txt)
        pos -= 0.05
    lines = []
    for group in groups:
        pbx = [x for x, d in group]
        pby = [d for x, d in group]
        lines.append(plot(pby, pbx, col.next()))
    #show()
    return lines
#
#
if __name__ == '__main__':
    #
    psyco.full()
    #
    ###------------------ To Modify ---------------------###
    #xml_dir = 'xml'                                       #
    #raw_dir = 'raw'                                       #
    xml_dir = 'test/xml'                                   #
    raw_dir = 'test/raw'                                   #
    xls = "selected.xls"                                   #
    DIR = "C:/datos_psearch"                               #
    #                                                      #
    fdr = 1                                                #
    #                                                      #
    #           114   115    116    117                    #
    isodata = [[1.000, 0.059, 0.002, 0.000],    # R114     #
               [0.020, 1.000, 0.056, 0.001],    # R115     #
               [0.000, 0.030, 1.000, 0.045],    # R116     #
               [0.000, 0.000, 0.035, 1.000]     # R117     #
               ]                                           #
    #                                                      #
    calidata = (0, 450)  # (center, window)                #
    #                                                      #
    key = 'decoy'                                          #
    ###--------------------------------------------------###
    #
    columns = (1, 2, 7, 4)  # (file, scan, z, prot)
    klasses = ['tar', 'dec']
    #
    xml_path = os.path.join(DIR, xml_dir)
    raw_path = os.path.join(DIR, raw_dir)
    xls_path = os.path.join(DIR, xls)
    #
    basedir, arch = os.path.split(xls_path)
    fxls = os.path.join(basedir, 'full_' + arch)
    #
    data = []
    n = 0
    for xml_fpath, raw_fpath in get_next_xml_raw(xml_path, raw_path):
        n += 1
        print 'file %i --> %s' % (n, os.path.basename(xml_fpath))
        part_data = excel_from_xml(xml_fpath, raw_fpath)
        if n == 1:
            data.extend(part_data)
        else:
            data.extend(part_data[1:])
    #
    save_list_of_lists(data, fxls)
    #
    # Dale functions start
    point = read_dict_from_list(data[1:], key)
    #
    # calculate filtering params and collection of selected items
    px, py, target_number, decoy_number, selected = find_best_line(fdr, point)
    #
    # select data groups based on their type
    groups = []
    for klass in klasses:
        points = [(x, d) for x, d, t in selected.values() if t == klass]
        groups.append(points)
    #
    # build texts for figure
    title_ = ""
    TXT1 = '\nD= %.2f   Xcor= %.2f    '
    TXT2 = '#target= %i   #decoy= %i'
    texts = [TXT1 % (px, py), TXT2 % (target_number, decoy_number)]
    #
    # show in console and in figure
    print (TXT1 + TXT2) % (px, py, target_number, decoy_number)
    lines2d = draw(groups, title_, texts)
    #
    # build new file only with new items
    dict_selected = filter_identifications(data, selected, key, columns=columns)
    print "%i points with fdr > %i" % (len(dict_selected), fdr)
    #
    # add ms2_ms3
    app = wx.PySimpleApp()
    dlg = wx.MessageDialog(None, "Do you want to add MS2_MS3?", style=wx.YES_NO)
    answer = dlg.ShowModal()
    if answer == wx.ID_YES:
        data_ms2_ms3 = get_data_ms2_ms3(data)
        dict_selected = add_ms2_ms3_from_list(data_ms2_ms3,
                                              dict_selected, headlines=(1,),
                                              columns=columns
                                              )
    print "there are %i points including ms2_ms3" % (len(dict_selected),)
    #
    # add quantification iTRAQ
    dlg = wx.MessageDialog(None, "Want to add iTRAQ data?", style=wx.YES_NO)
    answer = dlg.ShowModal()
    itraq_headlines = []
    if answer == wx.ID_YES:
        itraq_headlines = ['itraq_1', 'itraq_2', 'itraq_3', 'itraq_4',
                            'quant_1', 'quant_2', 'quant_3', 'quant_4']
        dict_selected = add_quant_from_raw(dict_selected, raw_path,
                                                          isodata, calidata)
    #
    # save all
    # headlines = get_headlines(arch, lines=(1,), DIR=DIR)
    headlines = data[0] + itraq_headlines
    save_dict_of_lists(dict_selected, xls_path, headlines)

    a = raw_input("enter tp exit")


## this doesn't works with TkAgg
##    linea = lines2d[0][0]
##    parent = linea.figure.canvas.GetParent()
##    parent.Destroy()
#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
version 3.0.4 5 December 2017
Search phosphorylated sequences in Dale lists
"""
#
peptides = ['ASDCFGTRTY',   # 01
            'ASDsFGFGJK',   # si
            'ASDbFGFGJK',   # si
            'DFKRJGHTOH',
            'DFKRJGHTOH',
            'ttsGHJ',       # no
            'sGHJuu',       # no
            'ASFKDTKBGF',
            'DFDSJRbCDR',   # si
            'DFDSJRsCDR',   # si 10
            'ASDtFGFGJK',   # si
            'ASDuFGFGJK',   # si
            'DFKRJGHTOH',
            'DFKRJGHTOH',
            'AsFTRDKbAR',   # si
            'ASFtRDKbAR',   # si
            'ASFKDTKBGF',
            'DFDSJRuCDR',   # si
            'DFDSJRtCDR',   # si
            'AKsSDNFRFL',   # 20
            'sdfASDADFG',
            'DKFGLbLFDL',
            'APLSDLGsRT',   # si
            'APLbDLGSRT',   # si
            'AKtSDNFRFL',
            'GTuuuuu',      # si
            'GTttttt',      # si
            'DKFGLuLFDL',
            'APLSDLGtRT',   # no
            'APLuDLGSRT',   # no
            'ASDbFGHsIH',   # no
            'ASDbFGHSIH',   # no
            'GPNbPDTANGF',  # si
            'GPNSPDtANGF',  # si
            'ASDbFGTHIJ',   # no
            'ASDSFGuHIJ',   # no
            'AsDbFGTHIJ',   # no
            'ASDSFGuHIJ'    # no
            ]

allowed = ['S', 'T', 's', 't', 'u', 'b']
combinations = dict(s=('S', 'b'),
                    t=('T', 'u'),
                    b=('s', 'S'),
                    u=('t', 'T'),
                    T=('t', 'u'),
                    S=('s', 'b')
                    )

def find_peptides(peptides):
    """"""
    mark = []
    seq = 0
    while seq < (len(peptides) - 1):
        seq += 1
        peptide1 = peptides[seq]
        peptide0 = peptides[seq - 1]
        #
        if peptide0.isupper():
            continue
        if peptide1.isupper():
            seq += 1
            continue
        if len(peptide0) != len(peptide1):
            continue
        if peptide0 == peptide1:
            continue
        if not are_alike(peptide0, peptide1):
            continue
        #
        A1 = ('s' in peptide0) or ('t' in peptide0)
        A2 = ('b' in peptide1) or ('u' in peptide1)
        AR1 = ('b' in peptide0) or ('u' in peptide0)
        AR2 = ('s' in peptide1) or ('t' in peptide1)

        if (A1 and A2) or (AR1 and AR2):
            mark.append(seq - 1)

    print('mark ', mark)
    return mark

def are_alike(one, other):
    """Compares two sequences for identity and same degree of ptms

    Search all combinations of amino acids in the same position.
    Returns False if there is nit the same number of lower and upper case
     chars in the two sequences.

    """
    length = len(one)
    counts_one = counts_other = 0
    for i in range(0, length):
        if one[i].islower():
            counts_one += 1
        if other[i].islower():
            counts_other += 1
        if one[i] != other[i]:
            if one[i] in allowed:
                if other[i] in combinations[one[i]]:
                    pass
                else:
                    return False
            else:
                return False

    if counts_one == counts_other:
        return True
    else:
        return False

def print_(peptides, marks):
    """"""
    seq = 0
    index = 0
    while seq < len(peptides):
        if marks[index] == seq:
            print(peptides[seq], '*')
            print(peptides[seq + 1], '*')

            if index < len(marks) - 1:
                index += 1
            seq += 2
        else:
            print(peptides[seq])
            seq += 1

if __name__ == '__main__':
    marks = find_peptides(peptides)
    print_(peptides, marks)

# produces mark=[1, 8, 10, 14, 17, 22, 25, 32]
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
functions to convert between different peptide representations
"""
#

translator = (('S[166]', 'S#'), ('S[69]', 'S@'),
              ('T[181]', 'T#'), ('T[83]', 'T@'),
              ('Y[243]', 'Y#'))

def from_brackets_to_ampersat(sequences, inverse=False):
    """Converts ptm bracket notation to symbol notation.

    S[166] -> S#
    if inverse is True it converts the other way around.

    """
    new_list = []
    (i, j) = (1, 0) if inverse else (0, 1)

    for sequence in sequences:
        for pair in translator:
            sequence.replace(pair[i], pair[j])
        new_list.append(sequence)

    return new_list

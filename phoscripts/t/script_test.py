__author__ = 'JOA'

import pickle
from time import time
from _tools.phoscripts.kinase_motifs import enlarge_sequences

db = "C:\\Blast\\DBs\\20090223Uniprot_sprot_human-release14_8.fasta"
OUT = 'out.pkl'
N = 10
sequences = open('sequences.txt', 'r').read()
sequences = sequences.split()

print sequences

one_prots = enlarge_sequences(sequences, dbase=db, all_seqs=False)
all_prots = enlarge_sequences(sequences, dbase=db, all_seqs=True)

#pickle.dump(one_prots, open('one_prots.pkl','w'))
#pickle.dump(all_prots, open('all_prots.pkl','w'))

p_one_prots = pickle.load(open('one_prots.pkl'))
p_all_prots = pickle.load(open('all_prots.pkl'))

print p_one_prots == one_prots
print p_all_prots == all_prots

for key, values in one_prots.iteritems():
    print '%20s   %s' %(key, values)

print '######'

for key, values in all_prots.iteritems():
    print '%20s   %s' %(key, values)


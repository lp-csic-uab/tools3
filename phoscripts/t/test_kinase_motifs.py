import unittest
import pickle
import os
from _tools.phoscripts.kinase_motifs import enlarge_sequences


__author__ = 'joaquin'

INPUT_FILE = os.path.join(os.path.dirname(__file__), 'sequences.txt')
EXPECTED_ONE = os.path.join(os.path.dirname(__file__), 'one_prots.pkl')
EXPECTED_ALL = os.path.join(os.path.dirname(__file__), 'all_prots.pkl')

class TestKinaseMotifs(unittest.TestCase):

    def setUp(self):
        self.db = "C:\\Blast\\DBs\\20090223Uniprot_sprot_human-release14_8.fasta"
        input_ = open(INPUT_FILE, 'r').read()
        self.input = input_.split()

    def test_enlarge_sequences_one(self):

        result_one = pickle.load(open(EXPECTED_ONE))
        one_prots = enlarge_sequences(self.input, dbase=self.db, all_seqs=False)

        self.assertDictEqual(one_prots, result_one)

    def test_enlarge_sequences_all(self):

        result_all = pickle.load(open(EXPECTED_ALL))
        all_prots = enlarge_sequences(self.input, dbase=self.db, all_seqs=True)

        self.assertEqual(all_prots, result_all)


if __name__ == '__main__':
    unittest.main()
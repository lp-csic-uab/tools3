from commons.mass import get_mass
from commons.mass import mass_atom
from kimblast.kblast_funcs import digest


def build_protein_mass_engine(protein_sequence, mass_error=0.5, max_charge=2,
                              enzyme=None, missed=0,
                              cys_cbm=1, metox=1):
    """Returns a function that search peptide ions derived from
     a protein sequence which match a given mass.

    mass_error: accepted difference with given mass (plus-minus)
    max_charge: mass charge for the ion
    enzyme: if set, matches only expected enzymatic fragments
            options: 'trypsin' (K, R (- KP, -RP))
                     'endolys' (K)
                     'glu_c'   (D, E)
                     'none'    ()
    missed: max number of mixed cleavages
    cys_cbm: consider possibility of carbamidomethylated cysteine
    metox: consider possibility of oxidised methionine

    """
    h_mass = mass_atom['H'][1]

    if enzyme:
        fragments = digest(protein_sequence, enzyme)
        if missed:
            miss_fragments = []
            for idx in range(0, len(fragments) - 1):
                miss_fragments.append(fragments[idx] + fragments[idx + 1])
            fragments.extend(miss_fragments)
        fragments = [frg for frg in fragments if len(frg) > 3]

        if cys_cbm:
            cys_list = [seq.replace('C', 'c')
                        for seq in fragments if 'C' in seq]
            fragments.extend(cys_list)
        if metox:
            met_list = [seq.replace('M', 'm')
                        for seq in fragments if 'm' in seq]
            fragments.extend(met_list)

        def search_enzymatic(ion_mass):
            """Gets protein substring (peptide) matching with ion mass.

            Only peptides derived from set enzyme are considered
            """
            collection = FragmentCollection()
            for fragment in fragments:
                mass = get_mass(fragment)
                ions = [(mass + (q * h_mass)) / q
                        for q in range(1, max_charge + 1)]
                for charge, test_mass in enumerate(ions):
                    error = abs(test_mass - ion_mass)
                    if error < mass_error:
                        start = protein_sequence.find(fragment)
                        stop = start + len(fragment)
                        collection.append((fragment, charge + 1,
                                           error,
                                           start + 1, stop))
            return collection

        return search_enzymatic
    else:
        def search(ion_mass):
            """Gets protein substring (peptide) matching with ion mass."""
            start = 0
            collection = FragmentCollection()
            finish = len(protein_sequence)

            while start + 3 < finish:
                for stop in range(start + 3, finish):
                    seq_ = protein_sequence[start:stop]
                    mass = get_mass(seq_)
                    if mass > ion_mass + 2:
                        break
                    else:
                        ions = [(mass + (q * h_mass)) / q
                                for q in range(1, max_charge + 1)]
                        for charge, test_mass in enumerate(ions):
                            error = abs(test_mass - ion_mass)
                            if error < mass_error:
                                collection.append((seq_, charge + 1,
                                                   error,
                                                   start + 1, stop))
                start += 1
            return collection

        return search


class FragmentCollection(list):
    def __init__(self):
        """Makes a list of lists object that pretty-prints itself"""
        list.__init__(self)

    def __repr__(self):
        full = []
        for item in self:
            seq, charge, err, start, stop = item
            text = 'T%i-%i +%i (%.3f)  >%s' % (start, stop, charge, err, seq)
            full.append(text)
        return '\n'.join(full)


class ProteinMaker(list):
    """A dedicated list for a protein sequence string"""
    def __init__(self, sequence_string):
        """Makes an instance of the protein"""
        super().__init__(sequence_string)

    def __repr__(self):
        if len(self) > 20:
            text = self[1:6] + '....' + self[-5:]
        else:
            text = self

        text = ''.join(text)
        length = len(self)

        return "ProteinMaker instance (length:%i)  >%s" % (length, text)

    def __str__(self):
        return ''.join(self)

    def __getitem__(self, item):
        if type(item) is slice:
            item = slice(item.start-1, item.stop)
        else:
            item = item-1
        
        return ''.join(super().__getitem__(item))

    def __call__(self, *args):
        """Gives fragment tag (from a sequence) or a sequence (from numbers)

        self('SADFD') -> T1-5
        self(2)   <=> self[2]     -> 'A'
        self(2,4) <=> self[2:4]   -> 'ADF'

        """
        if type(args[0]) is str:
            return self._find_fragment(args[0])
        elif type(args[0]) is int:
            if len(args) == 1:
                return self[args[0]]
            elif len(args) == 2:
                return self[args[0]:args[1]]
            
    def _find_fragment(self, seq):
        """String Find returning start and end index"""
        my_seq = ''.join(self)
        start = my_seq.find(seq)
        if start < 0:
            return -1, 0
        stop = start + len(seq)
        return start + 1, stop        


if __name__ == '__main__':
    from undercover.proteins import protein_1 as proq

    # masses = [654.48, 506.36,632.44,473.4,563.36,594.4,499.36,728.48,515.4]
    # search = build_protein_mass_engine(proq, enzyme=None) #'trypsin')
    #
    # for mass in masses:
    #
    #     col = search(mass)
    #     print '*** ', mass
    #     for item in col:
    #         print '%20s %i %.2f T%i-%i' % item
    a = ProteinMaker(proq)
    b = a('MRGSH')
    print(b)

#!/usr/bin/env python
# -*- coding: utf-8 -*-

from matplotlib import use, interactive

use('WXAgg')
interactive(False)
from pylab import hist, title, xlabel, ylabel, show, xlim
import re
from collections import defaultdict
#
try:
    from chapt import txt
except ImportError:
    txt = 'sample text archive not found'
#
"""
chap.txt es una constante txt=""string""
proveniente de un articulo.
Ultimo test=944 palabras con 0 repeticiones
            27614 palabras totales
            2492 palabras diferentes
            2945 sentences
            795  paragraphs
"""


def openf():
    """Opens a file and returns its contents

    For console input mode.
    For import use openfile()

    """
    while True:
        filename = raw_input('file to analyze =')
        if filename == '':
            text = txt
            break
        else:
            try:
                text = open(filename).read()
                break
            except IOError:
                print 'Error, Not a valid file'
    return text


def openfile(filepath):
    """Opens and reads a file returning its contents.

    (modo interativo con import)

    """
    try:
        texto = open(filepath).read()
    except IOError:
        texto = ''

    return texto


def listing(objtxt, start=1, stop=None):
    if stop is None:
        stop = start
    for i in range(start, stop + 1):
        print i, objtxt.frequencies[i]


class BeautyText:
    """Text clean of 'non-relevant' punctuation and blanks.

    non-relevant are numbers, parenthesis and line returns \n.
    To be fix = Converts puntos y aparte en punto y seguido

    """

    def __init__(self, text=" "):
        self.raw_text = text
        self.para = self.raw_text.lower().split('\n\n')
        self.txt = self.del_newline()
        self.txt, self.modules = self.del_separators()
        self.txt = self.del_comas()
        self.txt, self.numbers = self.del_numbers()
        self.txt = self.del_dots()
        self.txt = self.del_blanks()

    def del_newline(self):
        slist = [item.replace('\n', '') for item in self.para]
        txt = '\n\n'.join(slist)
        return txt

    def del_separators(self):
        pat = re.compile("[,;:]")
        return re.subn(pat, ' ', self.txt)

    def del_comas(self):
        pat = re.compile(r"[\}\{\]\[\"\\'´`|/¡!¿?()<>\^*+_\-#~=@%$‰]")
        #pat=re.compile(r"[^a-zA-Z0-9.\n]")
        return re.sub(pat, ' ', self.txt)

    def del_numbers(self):
        pat = re.compile(r"[0-9]+[.]?[0-9]*")
        return re.subn(pat, ' *n ', self.txt)

    def del_dots(self):
        pat = re.compile(r"[.]{2,30}")
        return re.sub(pat, '.', self.txt)

    def del_blanks(self):
        pat = re.compile(r"[ ]{2,30}")
        return re.sub(pat, ' ', self.txt)


class BrokenTxt:
    """Container with methods to deconstruct a text.

    a text deconstructed
        text = [paragraphs[sentences[words]]]
    a dictionary
        dictionary={word:repetitions}
    a frequency list
        frequencies = [..., [list of words with freq=i],...]
    and a frequency distribution
        distribution = [f_word1, f_word2, ....]

    """
    def __init__(self, text="", lang='us'):
        self.raw_text = text
        self.raw_text = self.singular(lang)
        self.text, self.paragraphs, self.sentences, self.words =\
            self.build_word_list()
        self.dictionary = self.count_words()
        self.frequencies, self.distribution = self.calc_freqs()

    def build_word_list(self):
        num_paragraphs = 0
        num_sentences = 0
        num_words = 0
        paragraphs = self.raw_text.split('\n\n')

        for paragraph in paragraphs:
            sentences = paragraph.rstrip(' .').split(
                '.')   # space in ' .' is important
            fcount = 0
            for sentence in sentences:
                sentence = sentence.strip()
                words = sentence.split()
                sentences[fcount] = words
                num_words += len(words)
                fcount += 1
            num_sentences += fcount
            paragraphs[num_paragraphs] = sentences[:num_sentences]
            num_paragraphs += 1

        paragraphs_cut = paragraphs[:num_paragraphs]
        return paragraphs_cut, num_paragraphs, num_sentences, num_words

    def singular(self, lang):
        """"""
        if lang == 'us':
            pat = re.compile(r"(ato|ss|sh|ch|ias)es\b")
            text = re.sub(pat, r'\1', self.raw_text)

            patst = r"(ng|t|r|b|l|d|g|p|m|k|n|w|c|ph|ve|\
                    rio|[ae]y|ea|a[gst]e|[pd]le|able|eave|\
                    ue|nce|ode|u[cdrs]e|[iy][lncmp]e)s\b"
            pat = re.compile(patst)
            text = re.sub(pat, r'\1', text)

            pat = re.compile(r"(.)ies\b")
            text = re.sub(pat, r'\1y', text)

        elif lang == 'sp':
            pat = re.compile(r"([aeiou])s\b")
            text = re.sub(pat, r'\1', self.raw_text)
        else:
            text = self.raw_text

        return text

    def count_words(self):
        """Build a dict of words and frequencies from a BrokenText instance.

        In works for the full text but could be useful to run it also
        for a paragraph or sentence

        """
        dictionary = defaultdict(int)
        for paragraph in self.text:
            for sentence in paragraph:
                for word in sentence:
                    dictionary[word] += 1
        return dictionary

    def calc_freqs(self):
        freq_dict = defaultdict(list)
        table_freqs = []

        for word, frequency in self.dictionary.items():
            freq_dict[frequency].append(word)
            table_freqs.append(frequency)
        return freq_dict, table_freqs

def test_run():
    text = openf()
    bt = BeautyText(text)
    print bt.txt[0:100]
    brock = BrokenTxt(bt.txt)
    max_index = max(brock.dictionary.values())
    listing(brock, 1)
    _ = hist(brock.distribution, range(1, max_index + 1))
    xlim(xmax=50)
    title('DISTR WORD FREQS IN TEXT')
    xlabel('repetitions')
    ylabel('number of words')
    show()

if __name__ == '__main__':
    test_run()

# -*- coding: utf-8 -*-
"""
:synopsis:   Basic classes for serials web_registration project.

:created:    2014-09-16

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  22014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    0.1 dev
:updated:    2014-09-23
"""

#===============================================================================
# Imports
#===============================================================================
# Django imports:
from django.views.generic.base import View, TemplateView
from django.http import HttpResponse, HttpResponseRedirect
from django.conf import settings

# Application imports:
from utilities import LoggerCls

# Python core imports:
import os


#===============================================================================
# Views basic classes definitions
#===============================================================================
class BasicHtmlPage(TemplateView):
    """Raw basic class for Pseudo-static web-page views management"""
    
    template_name = 'base.html'
    base_template = 'base.html'

    def __init__(self, *args, **kwargs):
        """
        Constructor.
        Simply adds a null request attribute to the instance
        """
        # Call the base implementation first:
        super(BasicHtmlPage, self).__init__(*args, **kwargs)
        #
        self.request = getattr(self, 'request', None)
    
    def get_context_data(self, **kwargs):
        """
        :returns: a default base context dictionary for the render, updated
        with any 'extra_context' dictionary present in ``self.kwargs``
        """
        kwargs.setdefault( 'extra_context', dict() )
        #
        # Call the base implementation first to get a context:
        base_context = super(BasicHtmlPage, self).get_context_data(**kwargs)
        # Add the default base context dictionary:
        base_context.update({'section_title': 'Oops!',
                             'base_template': self.base_template,
                             'css_sheet': 'default.css',
                             'APPNAME': settings.APPNAME,
                             })
        # Update with optional extra context dictionary defined in ``urlpatterns`` 
        # after as_view() call and passed in ``self.kwargs`` as `extra_context` key:
        base_context.update(kwargs['extra_context'])
        #
        return base_context


class FormsHtmlPage(BasicHtmlPage):
    """
    Basic class for web-page Forms views management.
    
    It implements the post method that makes use of Django middleware csrf
    (Cross Site Request Forgery) protection: each POST form in the template
    should contain the tag {% csrf_token %} 
    Also new is the implementation of a error logging system through.
    """
    
    form_class = None
    initial = dict()
    succes_url = None
    template_name = 'base.html'

    def __init__(self, *args, **kwargs):
        """
        Constructor
        """
        super(FormsHtmlPage, self).__init__(*args, **kwargs)
        #Prepare context attributes for form and errors:
        self._errors = LoggerCls()
        self.form = kwargs.get('form', None)
            
    def _put_error(self, key='Error', error_msg=''):
        """
        Add the `error_msg` to the ``self._errors`` defaultdict
        :param key: error type.
        :param error_msg: error message.
        """
        if not error_msg: 
            error_msg = 'Error in {0}'.format(str(self))
        self._errors.put_with_key(key, error_msg)
    
    @property
    def errors(self):
        """errors getter"""
        return self._errors
    @errors.deleter
    def errors(self):
        """errors deleter"""
        del self._errors.log
    
    def get_context_data(self, **kwargs):
        """
        :returns: a default base context dictionary updated with a form object,
        and form error information
        """
        # Call the base implementation first to get a context:
        context = super(FormsHtmlPage, self).get_context_data(**kwargs)
        # Update the context with error data (``self._errors`` contents):
        context['errors'] = self._errors().items()
        context['form'] = self.form
        del self.errors #Clear the ``self._errors`` defaultdict
        return context
    
    def get(self, request, *args, **kwargs):
        if not self.form and self.form_class:
            self.form = self.form_class(initial=self.initial)
        return super(BasicHtmlPage, self).get(request, *args, **kwargs)
    
    def post(self, request, *args, **kwargs):
        """
        Default handler method for POST requests.
        
        :returns: a :class:`HttpResponse` object whose content is  a context
        dictionary (from :method:`self.get_context_data`) rendered against a
        template.
        """
        if self.form:
            form_class = self.form.__class__
        else:
            form_class = self.form_class
        self.form = form_class(request.POST)
        if self.form.is_valid():
            self.form_save_hook(*args, **kwargs)
            if self.succes_url:
                return HttpResponseRedirect(self.succes_url)
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)
    
    def form_save_hook(self):
        """
        Override this method to do some extra processing
        """
        self.form.save(commit=True)
    
    def put(self, request, *args, **kwargs):
        """
        Default dummy handler method for PUT requests. PUT is a valid HTTP verb
        for creating (with a known URL) or editing an object, but browsers only
        support POST for now.
        """
        return self.post(request, *args, **kwargs)


class DinamicFile(View):
    """
    Abstract class for generating dynamic files:
    FileName and possibly other Dynamic data -> Some file
    """
    
    def __init__(self, *args, **kwargs):
        """
        Constructor.
        Simply adds a null request attribute to the instance
        """
        super(DinamicFile, self).__init__(*args, **kwargs)
        #
        self._name = ''
        self._ext = ''
        
        self.request = getattr(self, 'request', None)
        self.response = None
        self.saved = False
    
    # ``self.filename`` is a read-write property:
    @property
    def filename(self):
        return '{0}.{1}'.format(self._name, self._ext)
    @filename.setter
    def filename(self, filename):
        self._name, self._ext = os.path.splitext(filename)
        self.response = HttpResponse()
        self.response['Content-Disposition'] = 'attachment; filename="{0}"'.format(filename)
    
    def get(self, request, *args, **kwargs):
        self.filename = args[0]
        
        # Implement real get stuff in Subclasses!
        
        # The rest of get stuff is Subclass Responsibility
        
        # Return ``self.response``
        return self.response
    
    def post(self, request, *args, **kwargs):
        """
        The real parameters are in ``self.args`` and/or ``self.request.GET`` ->
        redirect to :method:``self.get``.
        So this should only be called from download forms.
        """
        return self.get(request, *args, **kwargs)    
    
    def save(self, request=None, filename=None):
        if request: self.request = request
        if filename: self.filename = filename
        #
        with open(self.filename, 'wb') as io_file:
            io_file.write(self.response.content)
        self.saved = True
        return self


# -*- coding: utf-8 -*-
"""
:synopsis:   Form classes for web_registration project.

:created:    2014-09-18

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    0.1 dev
:updated:    2014-09-23
"""

#===============================================================================
# Imports
#===============================================================================
# Django imports:
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

# Application imports:

# Python core imports:


#==============================================================================
# Form classes
#==============================================================================
class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'email', 'password')


class RegisterForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'email')
    
    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)
        # The ``username`` and ``email`` fields are required for registration:
        self.fields['email'].required = True
        # But not the other required fields from parent :class:`UserCreationForm`:
        self.fields['password1'].required = False
        self.fields['password2'].required = False


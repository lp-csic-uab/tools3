# -*- coding: utf-8 -*-
"""
:synopsis:   Django Admin for serials app.

:created:    2014-09-11

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    0.1 dev
:updated:    2014-09-16
"""

#===============================================================================
# Imports
#===============================================================================
# Django imports:
from django.contrib import admin

# Application imports:
from serials.models import Program, Version
from serials.forms import VersionForm

# Python core imports:


#===============================================================================
# Class definitions for Model Admin
#===============================================================================
class ProgramAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')
    list_filter = ('name',)
    search_fields = ('name',)


class VersionAdmin(admin.ModelAdmin):
    # Customize the Admin form:
    form = VersionForm #Use a custom ModelForm to include a custom ``serial_input`` field
    fieldsets = [
        ( 'Program information', {'fields': [('program', 'version')]} ), 
        ( 'Serial',              {'fields': ['serial_input']} ), 
        ( 'Available for users', {'fields': ['available'], 'classes':['collapse']} ), 
        ( 'Date information',    {'fields': [('creation_date', 'current')], 'classes':['collapse']} ),
    ]
    # readonly_fields = ('current',)
    
    # Customize the Admin change list:
    list_display = ('program', 'version', 'creation_date', 'current')
    list_filter = ('program', 'creation_date')
    search_fields = ('program',)
    
    def save_model(self, request, obj, form, change):
        """
        Set the object instance ``serial`` property to the value of the custom 
        ``serial_input`` field in the form
        """
        obj.serial = form.cleaned_data['serial_input'] 
        obj.save()


#===============================================================================
# Registering models with the django admin interface
#===============================================================================
admin.site.register(Program, ProgramAdmin)
admin.site.register(Version, VersionAdmin)
# -*- coding: utf-8 -*-
"""
:synopsis:   Form classes for serials app.

:created:    2014-09-18

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    0.1 dev
:updated:    2014-09-18
"""

#===============================================================================
# Imports
#===============================================================================
# Django imports:
from django import forms

# Application imports:
from serials.models import Version, UserProfile

# Python core imports:
from datetime import date


#==============================================================================
# Form classes
#==============================================================================
class VersionForm(forms.ModelForm):
    # Special form fields not in model:   
    serial_input = forms.CharField(max_length=128, min_length=3)
    
    class Meta:
        model = Version
        fields = ('program', 'version', 'serial_input', 'creation_date', 'current')
    
    def __init__(self, *args, **kwargs):
        # Set initial values for some special form fields:      
        model_instance = kwargs.get('instance')
        if model_instance: # A model instance is being modified:
            self.base_fields['serial_input'].initial = model_instance.serial #Get serial_input value from the model instance being modified
        else: # A new model instance -> use some default values:
            self.base_fields['serial_input'].initial = 'lpcsicuab'
            self.base_fields['creation_date'].initial = date.today()
        #
        super(VersionForm, self).__init__(*args, **kwargs)


class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ('institution', 'position', 'website',)


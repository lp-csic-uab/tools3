# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('serials', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='serials',
            old_name='serial',
            new_name='serial_hash',
        ),
    ]

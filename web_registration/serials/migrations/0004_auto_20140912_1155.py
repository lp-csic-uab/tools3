# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('serials', '0003_auto_20140910_1525'),
    ]

    operations = [
        migrations.RenameField(
            model_name='serial',
            old_name='serial_hash',
            new_name='serial_ciph',
        ),
    ]

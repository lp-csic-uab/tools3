# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('serials', '0004_auto_20140912_1155'),
    ]

    operations = [
        migrations.AlterField(
            model_name='serial',
            name='serial_ciph',
            field=models.CharField(max_length=175),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('serials', '0008_userprofile'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='institution',
            field=models.CharField(default=b'', max_length=250, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userprofile',
            name='position',
            field=models.CharField(default=b'', max_length=150, blank=True),
            preserve_default=True,
        ),
    ]

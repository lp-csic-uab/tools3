# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('serials', '0009_auto_20140918_1635'),
    ]

    operations = [
        migrations.AddField(
            model_name='version',
            name='available',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]

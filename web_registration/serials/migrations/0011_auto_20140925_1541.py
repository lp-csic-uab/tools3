# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('serials', '0010_version_available'),
    ]

    operations = [
        migrations.RenameField(
            model_name='version',
            old_name='latest',
            new_name='current',
        ),
    ]

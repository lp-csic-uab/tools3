# -*- coding: utf-8 -*-
"""
:synopsis:   Django Models for serials app.

:created:    2014-09-11

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    0.1 dev
:updated:    2014-09-16
"""

#===============================================================================
# Imports
#===============================================================================
# Django imports:
from django.db import models
from django.contrib.auth.models import User

# Python core imports:
import os
from Crypto.Cipher import AES


#===============================================================================
# Models definitions
#===============================================================================
class Program(models.Model):
    name = models.CharField(max_length=150)
    description = models.TextField(default='')
    #version_set : backward relation to the version model
    
    def __unicode__(self):
        return u'ID{0}: {1}'.format(self.pk, self.name)


class Version(models.Model):
    program = models.ForeignKey(Program)
    version = models.CharField(max_length=15) # Major.minor.patch.build
    serial_ciph = models.CharField(max_length=350) # Installer ciphered password/serial_number/...
    creation_date = models.DateField('date created')
    available = models.BooleanField(default=True)
    current = models.BooleanField(default=False)
    
    key = open( os.path.join(os.path.dirname(__file__),'key.txt'), 'r' ).read().strip()
    iv = open( os.path.join(os.path.dirname(__file__),'iv.txt'), 'r' ).read().strip()
    
    @property
    def serial(self):
        plain_text = self.decrypt(self.serial_ciph)
        return plain_text
    @serial.setter
    def serial(self, plain_text):
        if plain_text:
            self.serial_ciph = self.encrypt(plain_text)
    
    def __unicode__(self):
        return u'ID{0}: {1} v{2}'.format(self.pk, self.program.name, self.version)
    
    def encrypt(self, plain_unicode):
        """
        plain_unicode >-utf-8-> plain_text >-cipher-> cipher_text >-base64-> cipher_text64
        """
        # Encode in utf-8 to avoid problems with unicode characters in the cipher:
        plain_text = plain_unicode.encode('utf-8')
        # Add extra characters to round length to a multiple of 16:
        text_size = len(plain_text)
        padded_text = plain_text + '|' + ' '*(15 - text_size % 16)
        # Encrypt:
        cipher = AES.new(self.key, AES.MODE_CBC, self.iv)
        # Encode in base64 to avoid problems with non-ascii characters in the database:
        return cipher.encrypt(padded_text).encode('base64')
    
    def decrypt(self, cipher_text64):
        """
        cipher_text64 >-base64-> cipher_text >-cipher-> plain_text >-utf-8-> unicode_text
        """
        # Decode from base64:
        cipher_text = cipher_text64.decode('base64')
        cipher = AES.new(self.key, AES.MODE_CBC, self.iv)
        # Decrypt, and remove extra characters used to round length to a multiple of 16:
        plain_text = cipher.decrypt( cipher_text ).partition('|')[0]
        # Decode utf-8 back to unicode:
        return plain_text.decode('utf-8')
    
    def save(self, force_insert=False, force_update=False, using=None, 
        update_fields=None):
        # TO_DO: treat field `current`==True on saving 
        return super(Version, self).save(force_insert=force_insert, 
                                         force_update=force_update, 
                                         using=using, 
                                         update_fields=update_fields)


class UserProfile(models.Model):
    # This line is required. Links UserProfile to a User model instance:
    user = models.OneToOneField(User)
    # The additional attributes we wish to include:
    institution = models.CharField(max_length=250, blank=True, default='')
    position = models.CharField(max_length=150, blank=True, default='')
    website = models.URLField(blank=True)
    programs = models.ManyToManyField(Program)
 
    # Override the __unicode__() method to return out something meaningful!
    def __unicode__(self):
        return self.user.username

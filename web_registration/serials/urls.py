# -*- coding: utf-8 -*-
"""
:synopsis:   Django URL settings for serials app.
             When including this in another urls.py, please, put it in serials 
             namespace. Ex.: 
               url( r'^users/', include('serials.urls', namespace='serials') )

:created:    2014-09-17

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    0.1 dev
:updated:    2014-09-19
"""

#===============================================================================
# Imports
#===============================================================================
# Django imports:
from django.conf.urls import patterns, url

# Application imports:
from serials.views import UserMain


#===============================================================================
# URL patterns variable
#===============================================================================
urlpatterns = patterns('',
    url(r'^(?P<user_name>.*)$', UserMain.as_view(), 
        name='user_main'),
)

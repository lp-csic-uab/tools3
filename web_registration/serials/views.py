# -*- coding: utf-8 -*-
"""
:synopsis:   View classes for serials app.

:created:    2014-09-11

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    0.1 dev
:updated:    2014-09-19
"""

#===============================================================================
# Imports
#===============================================================================
# Django imports:
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

# Application imports:
from basic_classes import FormsHtmlPage


#===============================================================================
# Class definitions for Moldel Admin
#===============================================================================
class UserMain(FormsHtmlPage):
    
    template_name = 'serials/user_main.html'
    
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if kwargs['user_name'] == request.user.username:
            return super(FormsHtmlPage,self).dispatch(request, *args, **kwargs)
        else:
            return HttpResponseRedirect( reverse('serials:user_main', kwargs={'user_name': request.user.username}) )
    
    def get_context_data(self, **kwargs):
        """
        :returns: a default base context dictionary updated with a form object,
        and form error information
        """
        # Call the base implementation first to get a context:
        context = super(UserMain, self).get_context_data(**kwargs)
        # TO_DO: extra context stuff?
        return context

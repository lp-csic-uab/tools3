# -*- coding: utf-8 -*-
"""
:synopsis:   Utility classes for web_registration project.

:created:    2014-09-16

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    0.1 dev
:updated:    2014-09-16
"""

#===============================================================================
# Imports
#===============================================================================
# Django imports:

# Application imports:

# Python core imports:
from collections import defaultdict


#==============================================================================
# Accessory classes
#==============================================================================
class LoggerCls(object):
    def __init__(self, key_func=None, *func_args, **func_kwargs):
        """
        Constructor
        """
        
        self._log = defaultdict(list)
        self._crono_keys = list() #List of keys in self._log, in chronological order
        
        if not key_func:
            key_func = self.crono_index
        
        self.key_func = key_func
        self.func_args = func_args
        self.func_kwargs = func_kwargs
        
    def __call__(self, key=None):
        if key:
            return list( self._log.get(key, []) )
        else:
            return self.log
    
    @property
    def log(self):
        return self._log.copy()
    @log.setter
    def log(self, value):
        self.put(value)
    @log.deleter
    def log(self):
        self._log.clear()
        self._crono_keys = list()
    
    def put(self, value):
        key = self.key_func(*self.func_args, **self.func_kwargs)
        self.put_with_key(key, value)
        return key
        
    def put_with_key(self, key, value):
        self._log[key].append(value)
        self._crono_keys.append(key)
        
    def keys(self):
        return list(self._crono_keys)
    
    def crono_index(self, start=0):
        return len(self._crono_keys) + start


# -*- coding: utf-8 -*-
"""
:synopsis:   View classes for web_registration project.

:created:    2014-09-11

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    0.1 dev
:updated:    2014-09-23
"""

#===============================================================================
# Imports
#===============================================================================
# Django imports:
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth.views import password_reset

# Application imports:
from basic_classes import FormsHtmlPage
from forms import RegisterForm
#from web_registration.urls import extra_context_register

#===============================================================================
# Class definitions
#===============================================================================
class Register(FormsHtmlPage):
    
    form_class = RegisterForm
    template_name = 'register.html'
    succes_url = 'register_done'
    
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return HttpResponseRedirect( reverse('serials:user_main', kwargs={'user_name': request.user.username}) )
        else:
            return super(FormsHtmlPage,self).dispatch(request, *args, **kwargs)
    
    def form_save_hook(self, *args, **kwargs):
        new_user = self.form.save(commit=False)
        new_user.set_password( User.objects.make_random_password(length=20) )
        new_user.save()
        # Use django.contrib.auth.views.password_reset to generate and send an 
        # activation/confirmation link for the current user. It will use or 
        # POST data to populate a :class:`PasswordResetForm` instance, and use 
        # its :method:`save` method to generate the confirmation link and send 
        # the e-mail:
        password_reset(self.request, 
                       email_template_name='register_email.html', 
                       subject_template_name='register_subject.txt', 
                       post_reset_redirect='register_done', 
                       )


#===============================================================================
# Function definitions
#===============================================================================



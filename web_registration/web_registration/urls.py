# -*- coding: utf-8 -*-
"""
:synopsis:   Django URL settings for web_registration project.

:created:    2014-09-11

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    0.1 dev
:updated:    2014-09-23
"""

#===============================================================================
# Imports
#===============================================================================
# Django imports:
from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin

# Application imports:
from basic_classes import BasicHtmlPage
from views import Register


#===============================================================================
# URL patterns variable
#===============================================================================
admin.autodiscover()

#Common extra context dictionaries:
extra_context_reset_password = {
                                'base_template': 'base.html', 
                                'css_sheet': 'default.css', 
                                'APPNAME': settings.APPNAME, 
                                'section_title': 'Reset password', 
                                'class_login': r'current'
                                }
extra_context_register = {
                          'base_template': 'base.html', 
                          'css_sheet': 'default.css', 
                          'APPNAME': settings.APPNAME, 
                          'section_title': 'Register', 
                          'class_register': r'current'
                          }

urlpatterns = patterns('',
    url(r'^$', BasicHtmlPage.as_view(template_name='home_section.html'), 
        {'extra_context': {'section_title': 'Home Page', 
                           'class_home': r'current'}}, 
        name='home'
        ), 
    url(r'^about$', BasicHtmlPage.as_view(template_name='home_section.html'), 
       {'extra_context': {'section_title': 'About', 
                          'class_about': r'current'}}, 
        name='about'
        ), 
    url(r'^help$', BasicHtmlPage.as_view(template_name='home_section.html'), 
        {'extra_context': {'section_title': 'Help', 
                           'class_help': r'current'}}, 
        name='help'
        ), 
    url(r'^todo$', BasicHtmlPage.as_view(template_name='home_section.html'), 
        {'extra_context': {'section_title': 'To-Do', 
                           'class_todo': r'current'}}, 
        name='todo'
        ), 
    url(r'^login$', 'django.contrib.auth.views.login', 
        {'template_name': 'login.html', 
         'extra_context': {'base_template': 'base.html', 
                           'css_sheet': 'default.css', 
                           'APPNAME': settings.APPNAME, 
                           'section_title': 'Login', 
                           'class_login': r'current'}}, 
        name='login'
        ), 
    url(r'^logout$', 'django.contrib.auth.views.logout_then_login', 
        name='logout'
        ), 
    url(r'^password_reset$', 'django.contrib.auth.views.password_reset', 
        {'template_name': 'password_reset.html', 
         'email_template_name': 'password_reset_email.html', 
         'subject_template_name': 'password_reset_subject.txt', 
         'extra_context': extra_context_reset_password}, 
        name='password_reset'
        ), 
    url(r'^password_reset_done$', 'django.contrib.auth.views.password_reset_done', 
        {'template_name': 'password_reset_done.html', 
         'extra_context': extra_context_reset_password}, 
        name='password_reset_done'
        ), 
    url(r'^password_reset_confirm/(?P<uidb64>.*)/(?P<token>.*)$', 'django.contrib.auth.views.password_reset_confirm', 
        {'template_name': 'password_reset_confirm.html', 
         'extra_context': extra_context_reset_password}, 
        name='password_reset_confirm'
        ), 
    url(r'^password_reset_complete$', 'django.contrib.auth.views.password_reset_complete', 
        {'template_name': 'password_reset_complete.html', 
         'extra_context': extra_context_reset_password}, 
        name='password_reset_complete'
        ), 
    url(r'^registering$', Register.as_view(), 
        {'extra_context': extra_context_register}, 
        name='register'
        ), 
    url(r'^register_done$', 'django.contrib.auth.views.password_reset_done', 
        {'template_name': 'register_done.html', 
         'extra_context': extra_context_register}, 
        name='register_done'
        ), 
    url(r'^register_confirm/(?P<uidb64>.*)/(?P<token>.*)$', 'django.contrib.auth.views.password_reset_confirm', 
        {'template_name': 'register_confirm.html', 
         'extra_context': extra_context_register}, 
        name='register_confirm'
        ), 
    url(r'^register_complete$', 'django.contrib.auth.views.password_reset_complete', 
        {'template_name': 'register_complete.html', 
         'extra_context': extra_context_register}, 
        name='register_complete'
        ), 
    url( r'^admin/', include(admin.site.urls) ), 
    url( r'^users/', include('serials.urls', namespace='serials'), 
         { 'extra_context': {'class_users':r'current'} } 
         ), 
)

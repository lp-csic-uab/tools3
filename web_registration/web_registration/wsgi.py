# -*- coding: utf-8 -*-
"""
:synopsis:   WSGI config for web_registration project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/

:created:    2014-09-11

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    0.1 dev
:updated:    2014-09-15
"""

#==============================================================================
# Change these variables acording to server and project
#==============================================================================
_home_folder = '/home/web_registration/'
_web_folder = '/home/web_registration/www/'
_poject_name = 'web_registration'


#==============================================================================
# WSGI application handler definition, environment configuration and debugging
# options: 
#==============================================================================
import os, sys
#
sys.path.append(_web_folder + _poject_name)
sys.path.insert(1, _web_folder)
#
os.environ.setdefault("DJANGO_SETTINGS_MODULE", _poject_name + ".settings")
#os.environ['MPLCONFIGDIR'] = _web_folder + '.matplotlib'
os.environ['PYTHON_EGG_CACHE'] = _home_folder + 'tmp'

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
